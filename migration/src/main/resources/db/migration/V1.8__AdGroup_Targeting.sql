CREATE TABLE "target_browser" (
	"ad_group_id" int NOT NULL,
	"browser" varchar(50) NOT NULL,
	CONSTRAINT target_browser_pk PRIMARY KEY ("ad_group_id", "browser"),
	CONSTRAINT target_browser_fk0 FOREIGN KEY ("ad_group_id") REFERENCES "ad_group"("id") ON DELETE CASCADE
) WITH (
  OIDS=FALSE
);

CREATE TABLE "target_os" (
	"ad_group_id" int NOT NULL,
	"os" varchar(50) NOT NULL,
	CONSTRAINT target_os_pk PRIMARY KEY ("ad_group_id", "os"),
	CONSTRAINT target_os_fk0 FOREIGN KEY ("ad_group_id") REFERENCES "ad_group"("id") ON DELETE CASCADE
) WITH (
OIDS=FALSE
);

CREATE TABLE "target_device" (
	"ad_group_id" int NOT NULL,
	"device" varchar(50) NOT NULL,
	CONSTRAINT target_device_pk PRIMARY KEY ("ad_group_id", "device"),
	CONSTRAINT target_device_fk0 FOREIGN KEY ("ad_group_id") REFERENCES "ad_group"("id") ON DELETE CASCADE
) WITH (
OIDS=FALSE
);

CREATE TABLE "schedule_day" (
	"id" serial NOT NULL,
	"ad_group_id" int NOT NULL,
	"day_of_week" varchar(50) NOT NULL,
	CONSTRAINT schedule_day_pk PRIMARY KEY ("id"),
	CONSTRAINT schedule_day_fk0 FOREIGN KEY ("ad_group_id") REFERENCES "ad_group"("id") ON DELETE CASCADE
) WITH (
OIDS=FALSE
);

CREATE TABLE "schedule_hour" (
  "id" serial NOT NULL,
  "schedule_day_id" int NOT NULL,
  "start_hour" int NOT NULL,
  "end_hour" int NOT NULL,
  CONSTRAINT schedule_hour_pk PRIMARY KEY ("id"),
  CONSTRAINT schedule_hour_fk0 FOREIGN KEY ("schedule_day_id") REFERENCES "schedule_day"("id") ON DELETE CASCADE
) WITH (
OIDS=FALSE
);

GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA "public" TO "campaign_manager";

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA "public" TO "campaign_manager";