ALTER TABLE "user_role" DROP CONSTRAINT "user_role_fk0";

ALTER TABLE "user" RENAME TO "users";

ALTER TABLE "user_role" DROP CONSTRAINT "user_role_pk";

ALTER TABLE "user_role" ADD COLUMN "id" SERIAL NOT NULL;

ALTER TABLE "user_role" ADD CONSTRAINT "user_role_pk" PRIMARY KEY ("id");

ALTER TABLE "user_role" ADD CONSTRAINT "user_role_key" UNIQUE("user_id", "role");

ALTER TABLE "user_role" ADD CONSTRAINT "user_role_fk0" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE;

ALTER TABLE "users" RENAME COLUMN "email" TO "username";

ALTER TABLE "users" DROP CONSTRAINT "user_email_key";

ALTER TABLE "users" ADD CONSTRAINT "user_username_key" UNIQUE("username");

ALTER TABLE "users" ALTER COLUMN "password" TYPE VARCHAR(100);

GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA "public" TO "campaign_manager";