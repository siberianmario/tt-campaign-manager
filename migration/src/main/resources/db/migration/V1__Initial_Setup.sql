-- CREATE SCHEMA IF NOT EXISTS "campaign-manager";
--

CREATE TABLE "advertiser" (
	"id" serial NOT NULL,
	"name" varchar(50) NOT NULL,
	"description" TEXT,
	"site" varchar(200),
	CONSTRAINT advertiser_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "campaign" (
	"id" serial NOT NULL,
	"advertiser_id" int NOT NULL,
	"name" varchar(50) NOT NULL,
	"description" TEXT,
	"status_id" int NOT NULL,
	"start_date" timestamptz NOT NULL,
	"end_date" timestamptz NOT NULL,
	"lifetime_impression_cap" int NOT NULL,
	"lifetime_budget" money NOT NULL,
	CONSTRAINT campaign_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "ad_group" (
	"id" serial NOT NULL,
	"campaign_id" int NOT NULL,
	"name" varchar(50) NOT NULL,
	"description" TEXT,
	CONSTRAINT ad_group_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "creative" (
	"id" serial NOT NULL,
	"ad_group_id" int NOT NULL,
	"name" varchar(50) NOT NULL,
	"description" TEXT,
	"landing_page_url" varchar(200),
	"height" int NOT NULL,
	"width" int NOT NULL,
	"type" int NOT NULL,
	"asset" int NOT NULL,
	CONSTRAINT creative_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "campaign_status" (
	"id" serial NOT NULL,
	"status" varchar(10) NOT NULL,
	CONSTRAINT campaign_status_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "tracker" (
	"id" int NOT NULL,
	"creative_id" int NOT NULL,
	"body" TEXT NOT NULL,
	"type" int NOT NULL,
	CONSTRAINT tracker_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "tracker_type" (
	"id" int NOT NULL,
	"type" varchar(50) NOT NULL,
	CONSTRAINT tracker_type_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "creative_type" (
	"id" serial NOT NULL,
	"type" varchar(50) NOT NULL,
	CONSTRAINT creative_type_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "creative_asset" (
	"id" serial NOT NULL,
	"file_name" varchar(50) NOT NULL,
	"extension" varchar(10) NOT NULL,
	"body" bytea NOT NULL,
	CONSTRAINT creative_asset_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "user" (
	"id" serial NOT NULL,
	"email" varchar(50) NOT NULL UNIQUE,
	"password" varchar(50) NOT NULL,
	"role" int NOT NULL,
	"first_name" varchar(50) NOT NULL,
	"last_name" varchar(50) NOT NULL,
	CONSTRAINT user_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "user_role" (
	"id" serial NOT NULL,
	"role" varchar(10) NOT NULL,
	CONSTRAINT user_role_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);




ALTER TABLE "campaign" ADD CONSTRAINT "campaign_fk0" FOREIGN KEY ("advertiser_id") REFERENCES "advertiser"("id");
ALTER TABLE "campaign" ADD CONSTRAINT "campaign_fk1" FOREIGN KEY ("status_id") REFERENCES "campaign_status"("id");

ALTER TABLE "ad_group" ADD CONSTRAINT "ad_group_fk0" FOREIGN KEY ("campaign_id") REFERENCES "campaign"("id");

ALTER TABLE "creative" ADD CONSTRAINT "creative_fk0" FOREIGN KEY ("ad_group_id") REFERENCES "ad_group"("id");
ALTER TABLE "creative" ADD CONSTRAINT "creative_fk1" FOREIGN KEY ("type") REFERENCES "creative_type"("id");
ALTER TABLE "creative" ADD CONSTRAINT "creative_fk2" FOREIGN KEY ("asset") REFERENCES "creative_asset"("id");


ALTER TABLE "tracker" ADD CONSTRAINT "tracker_fk0" FOREIGN KEY ("creative_id") REFERENCES "creative"("id");
ALTER TABLE "tracker" ADD CONSTRAINT "tracker_fk1" FOREIGN KEY ("type") REFERENCES "tracker_type"("id");




ALTER TABLE "user" ADD CONSTRAINT "user_fk0" FOREIGN KEY ("role") REFERENCES "user_role"("id");


GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA "public" TO "campaign_manager";

GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA "public" TO "campaign_manager";