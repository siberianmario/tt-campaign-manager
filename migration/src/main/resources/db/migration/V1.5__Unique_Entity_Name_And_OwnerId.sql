ALTER TABLE "campaign" ADD CONSTRAINT "campaign_advertiser_id_name_key" UNIQUE("advertiser_id", "name");

ALTER TABLE "ad_group" ADD CONSTRAINT "ad_group_campaign_id_name_key" UNIQUE("campaign_id", "name");

ALTER TABLE "creative" ADD CONSTRAINT "creative_ad_group_id_name_key" UNIQUE("ad_group_id", "name");

CREATE SEQUENCE "tracker_id_seq";

ALTER TABLE "tracker" ALTER COLUMN "id" SET DEFAULT nextval('tracker_id_seq');

CREATE SEQUENCE "tracker_type_id_seq";

ALTER TABLE "tracker_type" ALTER COLUMN "id" SET DEFAULT nextval('tracker_type_id_seq');

GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA "public" TO "campaign_manager";