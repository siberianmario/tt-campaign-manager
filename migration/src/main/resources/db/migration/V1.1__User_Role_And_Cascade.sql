ALTER TABLE "advertiser" ADD CONSTRAINT "advertiser_name_key" UNIQUE("name");

ALTER TABLE "user" DROP COLUMN "role";

ALTER TABLE "user_role" ALTER COLUMN "role" TYPE varchar(50);

ALTER TABLE "user_role" DROP CONSTRAINT "user_role_pk";

ALTER TABLE "user_role" RENAME COLUMN "id" TO "user_id";

ALTER TABLE "user_role" ADD CONSTRAINT "user_role_pk" PRIMARY KEY ("user_id", "role");

ALTER TABLE "user_role" ADD CONSTRAINT "user_role_fk0" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE;


ALTER TABLE "campaign" DROP CONSTRAINT "campaign_fk0",
	ADD CONSTRAINT "campaign_fk0" FOREIGN KEY ("advertiser_id") REFERENCES "advertiser"("id") ON DELETE CASCADE;

ALTER TABLE "ad_group" DROP CONSTRAINT "ad_group_fk0",
	ADD CONSTRAINT "ad_group_fk0" FOREIGN KEY ("campaign_id") REFERENCES "campaign"("id") ON DELETE CASCADE;

ALTER TABLE "creative" DROP CONSTRAINT "creative_fk0",
	ADD CONSTRAINT "creative_fk0" FOREIGN KEY ("ad_group_id") REFERENCES "ad_group"("id") ON DELETE CASCADE;

ALTER TABLE "tracker" DROP CONSTRAINT "tracker_fk0",
	ADD CONSTRAINT "tracker_fk0" FOREIGN KEY ("creative_id") REFERENCES "creative"("id") ON DELETE CASCADE;