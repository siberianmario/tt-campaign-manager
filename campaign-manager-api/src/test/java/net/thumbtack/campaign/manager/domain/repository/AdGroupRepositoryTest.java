package net.thumbtack.campaign.manager.domain.repository;

import net.thumbtack.campaign.manager.configuration.RepositoryConfiguration;
import net.thumbtack.campaign.manager.domain.model.AdGroup;
import net.thumbtack.campaign.manager.domain.model.Advertiser;
import net.thumbtack.campaign.manager.domain.model.BrowserEnum;
import net.thumbtack.campaign.manager.domain.model.Campaign;
import net.thumbtack.campaign.manager.domain.model.CampaignStatus;
import net.thumbtack.campaign.manager.domain.model.CampaignStatusEnum;
import net.thumbtack.campaign.manager.domain.model.ScheduleDay;
import net.thumbtack.campaign.manager.domain.model.ScheduleHours;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static net.thumbtack.campaign.manager.utils.RepositoryTestUtils.*;
import static org.junit.Assert.assertEquals;

/**
 * AdGroup Repository Test.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(RepositoryConfiguration.class)
@Transactional
public class AdGroupRepositoryTest {

    @Autowired
    private AdvertiserRepository advertiserRepository;

    @Autowired
    private CampaignRepository campaignRepository;

    @Autowired
    private CampaignStatusRepository campaignStatusRepository;

    @Autowired
    private AdGroupRepository adGroupRepository;

    @Test
    public void testAdGroupSave() {
        Advertiser ad = createAdvertiserWithEntities();
        List<Campaign> campaigns = ad.getCampaigns();

        long count = campaignRepository.count();
        assertEquals(2, count);

        count = adGroupRepository.count();
        assertEquals(3, count);

        campaigns.remove(0);

        count = campaignRepository.count();
        assertEquals(1, count);

        count = adGroupRepository.count();
        assertEquals(1, count);
    }

    @Test
    public void testFindAdGroupsByCampaignId() {
        Advertiser ad = createAdvertiserWithEntities();
        List<Campaign> campaigns = ad.getCampaigns();

        Collection<AdGroup> fooAdGroupsFromDB = adGroupRepository.findByCampaignId(campaigns.get(0).getId());
        assertEquals(2, fooAdGroupsFromDB.size());

        Collection<AdGroup> barAdGroupsFromDB = adGroupRepository.findByCampaignId(campaigns.get(1).getId());
        assertEquals(1, barAdGroupsFromDB.size());
    }

    @Test
    public void testFindAdGroupsByCampaignIdAndName() {
        Advertiser ad = createAdvertiserWithEntities();
        List<Campaign> campaigns = ad.getCampaigns();

        AdGroup adGroupFromDB = adGroupRepository.findByCampaignIdAndName(campaigns.get(0).getId(), "fooAdGroup1");
        checkAdGroupFields(campaigns.get(0).getAdGroups().get(0), adGroupFromDB);
    }

    @Test
    public void testBrowsersSave() {
        Advertiser ad = createAdvertiserWithEntities();
        AdGroup adGroup = ad.getCampaigns().get(0).getAdGroups().get(0);
        adGroup.getBrowsers().add(BrowserEnum.CHROME);
        adGroup.getBrowsers().add(BrowserEnum.SAFARI);

        AdGroup adGroupFromDB = adGroupRepository.findOne(adGroup.getId());
        assertEquals(2, adGroupFromDB.getBrowsers().size());

        adGroup.getBrowsers().clear();
        adGroupFromDB = adGroupRepository.findOne(adGroup.getId());
        assertEquals(0, adGroupFromDB.getBrowsers().size());
    }

    @Test
    public void testScheduleSave() {
        Advertiser ad = createAdvertiserWithEntities();
        AdGroup adGroup = ad.getCampaigns().get(0).getAdGroups().get(0);

        ScheduleDay monday = new ScheduleDay(adGroup, DayOfWeek.MONDAY);
        ScheduleDay tuesday = new ScheduleDay(adGroup, DayOfWeek.TUESDAY);

        adGroup.getSchedule().add(monday);
        adGroup.getSchedule().add(tuesday);

        monday.getHours().add(new ScheduleHours(monday, 0, 12));
        monday.getHours().add(new ScheduleHours(monday, 20, 24));
        tuesday.getHours().add(new ScheduleHours(tuesday, 16, 20));

        AdGroup adGroupFromDB = adGroupRepository.findOne(adGroup.getId());
        assertEquals(2, adGroupFromDB.getSchedule().size());

        ScheduleDay mondayFromDB = adGroupFromDB.getSchedule().get(0);
        assertEquals(2, mondayFromDB.getHours().size());

        ScheduleDay tuesdayFromDB = adGroupFromDB.getSchedule().get(1);
        assertEquals(1, tuesdayFromDB.getHours().size());

        adGroup.getSchedule().clear();

        adGroupFromDB = adGroupRepository.findOne(adGroup.getId());
        assertEquals(0, adGroupFromDB.getSchedule().size());
    }

    private Advertiser createAdvertiserWithEntities() {
        Advertiser ad = buildAdvertiser();

        CampaignStatus statusDraft = campaignStatusRepository.findByStatus(CampaignStatusEnum.DRAFT);

        Campaign fooCamp = buildCampaign(ad, "fooCamp", statusDraft);
        Campaign barCamp = buildCampaign(ad, "barCamp", statusDraft);

        List<AdGroup> fooAdGroups = new ArrayList<>();
        fooAdGroups.add(new AdGroup(fooCamp, "fooAdGroup1", null));
        fooAdGroups.add(new AdGroup(fooCamp, "fooAdGroup2", null));
        fooCamp.setAdGroups(fooAdGroups);

        List<AdGroup> barAdGroups = new ArrayList<>();
        barAdGroups.add(new AdGroup(barCamp, "barAdGroup1", null));
        barCamp.setAdGroups(barAdGroups);

        List<Campaign> campaigns = new ArrayList<>();
        campaigns.add(fooCamp);
        campaigns.add(barCamp);
        ad.setCampaigns(campaigns);
        advertiserRepository.save(ad);

        return ad;
    }
}
