package net.thumbtack.campaign.manager.domain.repository;

import net.thumbtack.campaign.manager.configuration.RepositoryConfiguration;
import net.thumbtack.campaign.manager.domain.model.Advertiser;
import net.thumbtack.campaign.manager.domain.model.Campaign;
import net.thumbtack.campaign.manager.domain.model.CampaignStatus;
import net.thumbtack.campaign.manager.domain.model.CampaignStatusEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static net.thumbtack.campaign.manager.utils.RepositoryTestUtils.buildAdvertiser;
import static net.thumbtack.campaign.manager.utils.RepositoryTestUtils.buildCampaign;
import static org.junit.Assert.assertEquals;

/**
 * Campaign Repository Test.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(RepositoryConfiguration.class)
@Transactional
public class CampaignRepositoryTest {

    @Autowired
    private AdvertiserRepository advertiserRepository;

    @Autowired
    private CampaignRepository campaignRepository;

    @Autowired
    private CampaignStatusRepository campaignStatusRepository;

    @Test
    public void testSaveCampaign() {
        List<Advertiser> advertisers = createAdvertisersWithCampaigns();

        long count = advertiserRepository.count();
        assertEquals(2, count);

        count = campaignRepository.count();
        assertEquals(3, count);

        advertiserRepository.delete(advertisers.get(0));

        count = advertiserRepository.count();
        assertEquals(1, count);

        count = campaignRepository.count();
        assertEquals(1, count);
    }

    @Test
    public void testFindCampaignsByAdvertiserId() {
        List<Advertiser> advertisers = createAdvertisersWithCampaigns();

        Collection<Campaign> fooCampaignsFromDB = campaignRepository.findByAdvertiserId(advertisers.get(0).getId());
        assertEquals(2, fooCampaignsFromDB.size());

        Collection<Campaign> barCampaignsFromDB = campaignRepository.findByAdvertiserId(advertisers.get(1).getId());
        assertEquals(1, barCampaignsFromDB.size());
    }

    @Test
    public void testFindCampaignsByStatus() {
        createAdvertisersWithCampaigns();

        Collection<Campaign> draftCampaignsFromDB = campaignRepository.findByStatusStatus(CampaignStatusEnum.DRAFT);
        assertEquals(2, draftCampaignsFromDB.size());

        Collection<Campaign> activeCampaignsFromDB = campaignRepository.findByStatusStatus(CampaignStatusEnum.ACTIVE);
        assertEquals(1, activeCampaignsFromDB.size());
    }

    @Test
    public void testFindCampaignsByAdvertiserIdAndStatus() {
        List<Advertiser> advertisers = createAdvertisersWithCampaigns();

        Collection<Campaign> draftFooCampaignsFromDB = campaignRepository
                .findByAdvertiserIdAndStatusStatus(advertisers.get(0).getId(), CampaignStatusEnum.DRAFT);
        assertEquals(1, draftFooCampaignsFromDB.size());

    }

    private List<Advertiser> createAdvertisersWithCampaigns() {
        Advertiser foo = buildAdvertiser("foo");
        Advertiser bar = buildAdvertiser("bar");

        CampaignStatus statusDraft = campaignStatusRepository.findByStatus(CampaignStatusEnum.DRAFT);
        CampaignStatus statusActive = campaignStatusRepository.findByStatus(CampaignStatusEnum.ACTIVE);

        List<Campaign> fooCampaigns = new ArrayList<>();
        fooCampaigns.add(buildCampaign(foo, "FooCamp 1", statusDraft));
        fooCampaigns.add(buildCampaign(foo, "FooCamp 2", statusActive));
        foo.setCampaigns(fooCampaigns);
        advertiserRepository.save(foo);

        List<Campaign> barCampaigns = new ArrayList<>();
        barCampaigns.add(buildCampaign(bar, "BarCamp 1", statusDraft));
        bar.setCampaigns(barCampaigns);
        advertiserRepository.save(bar);

        List<Advertiser> advertisers = new ArrayList<>();
        advertisers.add(foo);
        advertisers.add(bar);
        return advertisers;
    }
}
