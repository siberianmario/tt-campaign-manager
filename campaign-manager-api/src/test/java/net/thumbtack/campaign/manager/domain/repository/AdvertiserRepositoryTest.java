package net.thumbtack.campaign.manager.domain.repository;

import net.thumbtack.campaign.manager.configuration.RepositoryConfiguration;
import net.thumbtack.campaign.manager.domain.model.Advertiser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static net.thumbtack.campaign.manager.utils.RepositoryTestUtils.buildAdvertiser;
import static net.thumbtack.campaign.manager.utils.RepositoryTestUtils.checkAdvertiserFields;
import static org.junit.Assert.*;

/**
 * Advertiser Repository Test.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(RepositoryConfiguration.class)
@Transactional
public class AdvertiserRepositoryTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdvertiserRepositoryTest.class);

    @Autowired
    private AdvertiserRepository advertiserRepository;

    @Test
    public void testSaveAdvertiser() {
        createAdvertiser();

        long count = advertiserRepository.count();
        assertEquals(1, count);
    }

    @Test
    public void testFindByName() {
        Advertiser ad = createAdvertiser();

        Advertiser adFromDB = advertiserRepository.findByName(ad.getName());
        checkAdvertiserFields(ad, adFromDB);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testUniqueNameConstraint() {
        createAdvertiser();
        createAdvertiser();
    }

    @Test
    public void testDeleteAdvertiser() {
        Advertiser foo = createAdvertiser("Foo");
        Advertiser bar = createAdvertiser("Bar");

        long count = advertiserRepository.count();
        assertEquals(2, count);

        advertiserRepository.delete(foo);

        Advertiser fooFromDB = advertiserRepository.findByName(foo.getName());
        assertNull(fooFromDB);

        Advertiser barFromDB = advertiserRepository.findByName(bar.getName());
        assertNotNull(barFromDB);
    }

    @Test
    public void testUpdateAdvertiser() {
        Advertiser foo = createAdvertiser("Foo");
        advertiserRepository.save(foo);
        Advertiser fooFromDB = advertiserRepository.findOne(foo.getId());
        checkAdvertiserFields(foo, fooFromDB);

        foo.setDescription("Some foo description");
        foo.setSite("foo.com");
        advertiserRepository.save(foo);
        fooFromDB = advertiserRepository.findOne(foo.getId());
        checkAdvertiserFields(foo, fooFromDB);
    }

    private Advertiser createAdvertiser() {
        Advertiser advertiser = advertiserRepository.save(buildAdvertiser());
        LOGGER.debug("Saved Advertiser {}", advertiser);
        return advertiser;
    }

    private Advertiser createAdvertiser(String name) {
        Advertiser advertiser = advertiserRepository.save(buildAdvertiser(name));
        LOGGER.debug("Saved Advertiser {}", advertiser);
        return advertiser;
    }
}
