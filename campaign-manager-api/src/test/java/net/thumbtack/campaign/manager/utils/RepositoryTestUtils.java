package net.thumbtack.campaign.manager.utils;

import net.thumbtack.campaign.manager.domain.model.AdGroup;
import net.thumbtack.campaign.manager.domain.model.Advertiser;
import net.thumbtack.campaign.manager.domain.model.Campaign;
import net.thumbtack.campaign.manager.domain.model.CampaignStatus;
import net.thumbtack.campaign.manager.domain.model.Creative;
import net.thumbtack.campaign.manager.domain.model.CreativeAsset;
import net.thumbtack.campaign.manager.domain.model.CreativeType;
import net.thumbtack.campaign.manager.domain.model.User;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Repository Test Utils.
 */
public class RepositoryTestUtils {

    // Advertiser utils

    public static Advertiser buildAdvertiser() {
        return new Advertiser("Foo", "Foo Inc. company", "foo.com");
    }

    public static Advertiser buildAdvertiser(String name) {
        return new Advertiser(name, "Test company", "test.com");
    }

    public static void checkAdvertiserFields(Advertiser ad1, Advertiser ad2) {
        assertEquals(ad1.getId(), ad2.getId());
        assertEquals(ad1.getName(), ad2.getName());
        assertEquals(ad1.getDescription(), ad2.getDescription());
        assertEquals(ad1.getSite(), ad2.getSite());
    }


    // User utils

    public static User buildUser() {
        return new User("user", "user", "John", "Doe");
    }

    public static void checkUserFields(User user1, User user2) {
        assertEquals(user1.getId(), user2.getId());
        assertEquals(user1.getFirstName(), user2.getFirstName());
        assertEquals(user1.getLastName(), user2.getLastName());
        assertEquals(user1.getUsername(), user2.getUsername());
        assertEquals(user1.getPassword(), user2.getPassword());
        assertEquals(user1.getRoles().size(), user2.getRoles().size());
    }



    // Campaign utils

    public static Campaign buildCampaign(Advertiser advertiser, String name, CampaignStatus status) {
        Calendar calendar = Calendar.getInstance();
        Date start = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date end = calendar.getTime();
        return new Campaign(
                advertiser,
                name,
                "Test campaign description",
                status,
                new Timestamp(start.getTime()),
                new Timestamp(end.getTime()),
                1_000_000,
                new BigDecimal("1000.00"));
    }

    public static void checkCampaignFields(Campaign c1, Campaign c2) {
        assertEquals(c1.getId(), c2.getId());
        assertEquals(c1.getName(), c2.getName());
        assertEquals(c1.getDescription(), c2.getDescription());
        assertEquals(c1.getStatus(), c2.getStatus());
        assertEquals(c1.getStartDate(), c2.getStartDate());
        assertEquals(c1.getEndDate(), c2.getEndDate());
        assertEquals(c1.getLifetimeImpCap(), c2.getLifetimeImpCap());
        assertEquals(c1.getLifetimeBudget(), c2.getLifetimeBudget());
    }

    public static void checkAdGroupFields(AdGroup ag1, AdGroup ag2) {
        assertEquals(ag1.getId(), ag2.getId());
        assertEquals(ag1.getName(), ag2.getName());
        assertEquals(ag1.getDescription(), ag2.getDescription());
    }

    public static Creative buildCreative(AdGroup adGroup, String name, CreativeType type) {
        return new Creative(
                adGroup,
                name,
                null,
                "testlanding.com",
                500,
                200,
                type,
                new CreativeAsset(name, ".png", new byte[]{125,100})
        );
    }

    public static void checkCreativeFields(Creative crtv1, Creative crtv2) {
        assertEquals(crtv1.getId(), crtv2.getId());
        assertEquals(crtv1.getName(), crtv2.getName());
        assertEquals(crtv1.getDescription(), crtv2.getDescription());
        assertEquals(crtv1.getLandingPageUrl(), crtv2.getLandingPageUrl());
        assertEquals(crtv1.getHeight(), crtv2.getHeight());
        assertEquals(crtv1.getWidth(), crtv2.getWidth());
        assertEquals(crtv1.getType(), crtv2.getType());
        assertEquals(crtv1.getAsset(), crtv2.getAsset());
    }
}
