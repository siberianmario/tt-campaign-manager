package net.thumbtack.campaign.manager.domain.repository;

import net.thumbtack.campaign.manager.configuration.RepositoryConfiguration;
import net.thumbtack.campaign.manager.domain.model.AdGroup;
import net.thumbtack.campaign.manager.domain.model.Advertiser;
import net.thumbtack.campaign.manager.domain.model.Campaign;
import net.thumbtack.campaign.manager.domain.model.CampaignStatus;
import net.thumbtack.campaign.manager.domain.model.CampaignStatusEnum;
import net.thumbtack.campaign.manager.domain.model.Creative;
import net.thumbtack.campaign.manager.domain.model.CreativeType;
import net.thumbtack.campaign.manager.domain.model.CreativeTypeEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

import static net.thumbtack.campaign.manager.utils.RepositoryTestUtils.*;
import static org.junit.Assert.assertEquals;

/**
 * Creative Repository Test.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(RepositoryConfiguration.class)
@Transactional
public class CreativeRepositoryTest {

    @Autowired
    private AdvertiserRepository advertiserRepository;

    @Autowired
    private CampaignStatusRepository campaignStatusRepository;

    @Autowired
    private AdGroupRepository adGroupRepository;

    @Autowired
    private CreativeRepository creativeRepository;

    @Autowired
    private CreativeTypeRepository creativeTypeRepository;

    @Test
    public void testCreativeSave() {
        Advertiser ad = createAdvertiserWithEntities();
        List<AdGroup> adGroups = ad.getCampaigns().get(0).getAdGroups();

        assertEquals(2, adGroupRepository.count());
        assertEquals(3, creativeRepository.count());

        adGroups.remove(0);

        assertEquals(1, adGroupRepository.count());
        assertEquals(1, creativeRepository.count());
    }

    @Test
    public void testFindByAdGroup() {
        Advertiser ad = createAdvertiserWithEntities();
        AdGroup fooAdGroup = ad.getCampaigns().get(0).getAdGroups().get(0);

        Collection<Creative> creatives = creativeRepository.findByAdGroupId(fooAdGroup.getId());
        assertEquals(2, creatives.size());
    }

    @Test
    public void testFindByAdGroupIdAndName() {
        Advertiser ad = createAdvertiserWithEntities();
        AdGroup fooAdGroup = ad.getCampaigns().get(0).getAdGroups().get(0);
        Creative fooCreative = fooAdGroup.getCreatives().get(0);

        Creative creativeFromDB = creativeRepository.findByAdGroupIdAndName(fooAdGroup.getId(), fooCreative.getName());
        checkCreativeFields(fooCreative, creativeFromDB);
    }

    private Advertiser createAdvertiserWithEntities() {
        Advertiser ad = buildAdvertiser();

        CampaignStatus statusDraft = campaignStatusRepository.findByStatus(CampaignStatusEnum.DRAFT);
        Campaign campaign = buildCampaign(ad, "campaign", statusDraft);

        AdGroup fooAdGroup = new AdGroup(campaign, "foo", null);
        AdGroup barAdGroup = new AdGroup(campaign, "bar", null);

        CreativeType typeImage = creativeTypeRepository.findByType(CreativeTypeEnum.IMAGE);

        fooAdGroup.getCreatives().add(buildCreative(fooAdGroup, "fooCreative1", typeImage));
        fooAdGroup.getCreatives().add(buildCreative(fooAdGroup, "fooCreative2", typeImage));
        barAdGroup.getCreatives().add(buildCreative(barAdGroup, "barCreative", typeImage));

        campaign.getAdGroups().add(fooAdGroup);
        campaign.getAdGroups().add(barAdGroup);

        ad.getCampaigns().add(campaign);
        advertiserRepository.save(ad);
        return ad;
    }
}
