package net.thumbtack.campaign.manager.domain.repository;

import net.thumbtack.campaign.manager.configuration.RepositoryConfiguration;
import net.thumbtack.campaign.manager.domain.model.AdGroup;
import net.thumbtack.campaign.manager.domain.model.Advertiser;
import net.thumbtack.campaign.manager.domain.model.Campaign;
import net.thumbtack.campaign.manager.domain.model.CampaignStatus;
import net.thumbtack.campaign.manager.domain.model.CampaignStatusEnum;
import net.thumbtack.campaign.manager.domain.model.Creative;
import net.thumbtack.campaign.manager.domain.model.CreativeType;
import net.thumbtack.campaign.manager.domain.model.CreativeTypeEnum;
import net.thumbtack.campaign.manager.domain.model.Tracker;
import net.thumbtack.campaign.manager.domain.model.TrackerType;
import net.thumbtack.campaign.manager.domain.model.TrackerTypeEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static net.thumbtack.campaign.manager.utils.RepositoryTestUtils.*;
import static org.junit.Assert.assertEquals;

/**
 * Tracker Repository Test.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(RepositoryConfiguration.class)
@Transactional
public class TrackerRepositoryTest {

    @Autowired
    private AdvertiserRepository advertiserRepository;

    @Autowired
    private CampaignStatusRepository campaignStatusRepository;

    @Autowired
    private AdGroupRepository adGroupRepository;

    @Autowired
    private CreativeRepository creativeRepository;

    @Autowired
    private CreativeTypeRepository creativeTypeRepository;

    @Autowired
    private TrackerRepository trackerRepository;

    @Autowired
    private TrackerTypeRepository trackerTypeRepository;

    @Test
    public void testTrackerSaveDelete() {
        Advertiser ad = createAdvertiserWithEntities();
        List<Creative> creatives = ad.getCampaigns().get(0).getAdGroups().get(0).getCreatives();

        assertEquals(3, trackerRepository.count());

        creatives.remove(0);

        assertEquals(1, trackerRepository.count());
    }

    @Test
    public void testGetTrackersByCreativeId() {
        Advertiser ad = createAdvertiserWithEntities();
        List<Creative> creatives = ad.getCampaigns().get(0).getAdGroups().get(0).getCreatives();

        assertEquals(2, trackerRepository.findByCreativeId(creatives.get(0).getId()).size());
    }

    private Advertiser createAdvertiserWithEntities() {
        Advertiser ad = buildAdvertiser();

        CampaignStatus statusDraft = campaignStatusRepository.findByStatus(CampaignStatusEnum.DRAFT);
        Campaign campaign = buildCampaign(ad, "campaign", statusDraft);

        AdGroup adGroup = new AdGroup(campaign, "adGroup", null);

        CreativeType typeImage = creativeTypeRepository.findByType(CreativeTypeEnum.IMAGE);

        Creative fooCreative = buildCreative(adGroup, "fooCreative", typeImage);
        Creative barCreative = buildCreative(adGroup, "barCreative", typeImage);

        TrackerType trackerType = trackerTypeRepository.findByType(TrackerTypeEnum.HTML_JS);

        fooCreative.getTrackers().add(new Tracker(fooCreative, "foo body 1", trackerType));
        fooCreative.getTrackers().add(new Tracker(fooCreative, "foo body 2", trackerType));
        barCreative.getTrackers().add(new Tracker(barCreative, "bar body", trackerType));

        adGroup.getCreatives().add(fooCreative);
        adGroup.getCreatives().add(barCreative);
        campaign.getAdGroups().add(adGroup);
        ad.getCampaigns().add(campaign);
        advertiserRepository.save(ad);
        return ad;
    }
}
