package net.thumbtack.campaign.manager.domain.repository;

import net.thumbtack.campaign.manager.configuration.RepositoryConfiguration;
import net.thumbtack.campaign.manager.domain.model.User;
import net.thumbtack.campaign.manager.domain.model.UserRole;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static net.thumbtack.campaign.manager.utils.RepositoryTestUtils.buildUser;
import static net.thumbtack.campaign.manager.utils.RepositoryTestUtils.checkUserFields;
import static org.junit.Assert.assertEquals;

/**
 * User Repository Test.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(RepositoryConfiguration.class)
@Transactional
public class UserRoleRepositoryTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserRoleRepositoryTest.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Test
    public void testSaveUser() {
        User user = buildUser();
        UserRole roleUser = new UserRole(user, "ROLE_USER");
        UserRole roleAdmin = new UserRole(user, "ROLE_ADMIN");
        Set<UserRole> roles = new HashSet<>();
        roles.add(roleUser);
        roles.add(roleAdmin);
        user.setRoles(roles);
        userRepository.save(user);

        long count = userRepository.count();
        assertEquals(1, count);

        count = userRoleRepository.count();
        assertEquals(2, count);

        User userFromDB = userRepository.findByUsername("user");
        checkUserFields(user, userFromDB);

        Collection<UserRole> rolesFromDB = userRoleRepository.findByUserUsername(user.getUsername());
        assertEquals(roles.size(), rolesFromDB.size());
    }

    @Test
    public void testUpdateUserRoles() {
        User user = buildUser();
        user.getRoles().add(new UserRole(user, "ROLE_USER"));
        userRepository.save(user);

        Collection<UserRole> rolesFromDB = userRoleRepository.findByUserUsername(user.getUsername());
        assertEquals(1, rolesFromDB.size());

        user.getRoles().add(new UserRole(user, "ROLE_MANAGER"));

        rolesFromDB = userRoleRepository.findByUserUsername(user.getUsername());
        assertEquals(2, rolesFromDB.size());

        user.getRoles().removeIf(userRole -> userRole.getRole().equals("ROLE_USER"));

        rolesFromDB = userRoleRepository.findByUserUsername(user.getUsername());
        assertEquals(1, rolesFromDB.size());
    }
}
