package net.thumbtack.campaign.manager.web.form;

import net.thumbtack.campaign.manager.domain.model.TrackerTypeEnum;

/**
 * Form for creating Tracker.
 */
public class TrackerCreateForm {

    private String body;

    private TrackerTypeEnum type;

    public TrackerCreateForm() {
    }

    public String getBody() {
        return body;
    }

    public TrackerTypeEnum getType() {
        return type;
    }

    @Override
    public String toString() {
        return "TrackerCreateForm{" +
                "body='" + body + '\'' +
                ", type=" + type +
                '}';
    }
}
