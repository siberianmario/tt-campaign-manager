package net.thumbtack.campaign.manager.web.utils;

import net.thumbtack.campaign.manager.domain.model.User;
import net.thumbtack.campaign.manager.domain.model.UserRole;
import net.thumbtack.campaign.manager.web.form.UserCreateForm;
import net.thumbtack.campaign.manager.web.form.UserRoleUpdateForm;
import net.thumbtack.campaign.manager.web.form.UserUpdateForm;

import java.util.HashSet;
import java.util.Set;

/**
 * User Utils.
 */
public class UserUtils {

    /**
     * Build User object with CreateForm parameters.
     * @param userForm CreateForm with parameters
     * @return new Advertiser
     */
    public static User buildUser(UserCreateForm userForm) {
        ValidationUtils.validateUserCreateForm(userForm);
        User user = new User(userForm.getUsername(), userForm.getPassword(),
                userForm.getFirstName(), userForm.getLastName());
        if (userForm.getRoles() != null) {
            Set<UserRole> roles = new HashSet<>();
            userForm.getRoles().forEach(role -> roles.add(new UserRole(user, role)));
            user.setRoles(roles);
        }
        return user;
    }

    /**
     * Update User with non-null UpdateForm parameters.
     * @param user User to update
     * @param userForm updated User
     * @return
     */
    public static User updateUser(User user, UserUpdateForm userForm) {
        ValidationUtils.validateUserUpdateForm(userForm);
        if (userForm.getPassword() != null) {
            user.setPassword(userForm.getPassword().get());
        }
        if (userForm.getFirstName() != null) {
            user.setFirstName(userForm.getFirstName().get());
        }
        if (userForm.getLastName() != null) {
            user.setLastName(userForm.getLastName().get());
        }
        return user;
    }

    /**
     * Builds set of UserRoles from RoleForm.
     * @param user owner of roles
     * @param roleForm received roles
     * @return set of UserRoles
     */
    public static Set<UserRole> buildUserRoles(User user, UserRoleUpdateForm roleForm) {
        ValidationUtils.validateUserRoles(roleForm.getRoles());
        Set<UserRole> userRoles = new HashSet<>();
        roleForm.getRoles().forEach(role -> userRoles.add(new UserRole(user, role)));
        return userRoles;
    }
}
