package net.thumbtack.campaign.manager.domain.repository;

import net.thumbtack.campaign.manager.domain.model.Tracker;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

/**
 * Repository to access Tracker data.
 */
public interface TrackerRepository extends CrudRepository<Tracker, Integer> {

    /**
     * Find all Trackers by Creative Id.
     * @param id Id of the Creative
     * @return Collection of Creative's Trackers
     */
    Collection<Tracker> findByCreativeId(Integer id);
}
