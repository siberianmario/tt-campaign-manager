package net.thumbtack.campaign.manager.web.utils;


import net.thumbtack.campaign.manager.domain.model.Tracker;
import net.thumbtack.campaign.manager.web.form.TrackerCreateForm;
import net.thumbtack.campaign.manager.web.form.TrackerUpdateForm;

/**
 * Tracker Utils.
 */
public class TrackerUtils {

    /**
     * Build Tracker object with CreateForm parameters.
     * @param form CreateForm with parameters
     * @return new Tracker
     */
    public static Tracker buildTracker(TrackerCreateForm form) {
        ValidationUtils.validateTrackerCreateForm(form);
        return new Tracker(null, form.getBody(), null);
    }

    /**
     * Update Tracker object with UpdateForm parameters.
     * @param form UpdateForm with parameters
     * @return updated Tracker
     */
    public static Tracker updateTracker(Tracker tracker, TrackerUpdateForm form) {
        ValidationUtils.validateTrackerUpdateForm(form);
        if (form.getBody() != null) {
            form.getBody().ifPresent(tracker::setBody);
        }
        return tracker;
    }
}
