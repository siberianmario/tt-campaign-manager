package net.thumbtack.campaign.manager.domain.repository;

import net.thumbtack.campaign.manager.domain.model.Advertiser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Repository interface to access Advertiser data.
 */
public interface AdvertiserRepository extends PagingAndSortingRepository<Advertiser, Integer> {

    /**
     * Find Advertiser by it's name.
     * @param name Name of the advertiser
     * @return Advertiser with specified name
     */
    Advertiser findByName(String name);

    /**
     * Find page of Advertisers by name with like and ignore case.
     * @param name Name of the advertiser
     * @return Advertiser with specified name
     */
    Page<Advertiser> findByNameIgnoreCaseContaining(String name, Pageable var1);
}
