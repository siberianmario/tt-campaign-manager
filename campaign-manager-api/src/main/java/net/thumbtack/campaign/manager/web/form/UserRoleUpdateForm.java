package net.thumbtack.campaign.manager.web.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Set;

/**
 * Form for updating UserRoles.
 */
@ApiModel
public class UserRoleUpdateForm {

    @ApiModelProperty(notes = "User's roles")
    private Set<String> roles;

    public UserRoleUpdateForm() {
    }

    public Set<String> getRoles() {
        return roles;
    }

    @Override
    public String toString() {
        return "UserRoleUpdateForm{" +
                "roles=" + roles +
                '}';
    }
}
