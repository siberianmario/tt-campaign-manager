package net.thumbtack.campaign.manager.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * A top level entity for CreativeAsset.
 */
@Entity
@Table(name = "creative_asset")
public class CreativeAsset {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The id of the CreativeAsset", required = true)
    private Integer id;

    @Column(name = "file_name")
    @ApiModelProperty(notes = "The file name of the CreativeAsset", required = true)
    private String fileName;

    @ApiModelProperty(notes = "The extension of the CreativeAsset", required = true)
    private String extension;

    @JsonIgnore
    @ApiModelProperty(notes = "The body of the CreativeAsset", required = true)
    private byte[] body;

    public CreativeAsset() {
    }

    public CreativeAsset(String fileName, String extension, byte[] body) {
        this.fileName = fileName;
        this.extension = extension;
        this.body = body;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "CreativeAsset{" +
                "id=" + id +
                ", fileName='" + fileName + '\'' +
                ", extension='" + extension + '\'' +
                ", bodySize='" + body.length + '\'' +
                '}';
    }
}
