package net.thumbtack.campaign.manager.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * A top level entity for Tracker.
 */
@Entity
@Table(name = "tracker")
public class Tracker {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "creative_id")
    private Creative creative;

    private String body;

    @OneToOne
    @JoinColumn(name = "type")
    private TrackerType trackerType;

    protected Tracker() {
    }

    public Tracker(Creative creative, String body, TrackerType trackerType) {
        this.creative = creative;
        this.body = body;
        this.trackerType = trackerType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Creative getCreative() {
        return creative;
    }

    public void setCreative(Creative creative) {
        this.creative = creative;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public TrackerType getTrackerType() {
        return trackerType;
    }

    public void setTrackerType(TrackerType trackerType) {
        this.trackerType = trackerType;
    }

    @Override
    public String toString() {
        return "Tracker{" +
                "id=" + id +
                ", trackerType=" + trackerType +
                ", body='" + body + '\'' +
                '}';
    }
}
