package net.thumbtack.campaign.manager.web.responses;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.thumbtack.campaign.manager.domain.model.User;

import java.util.HashSet;
import java.util.Set;

/**
 * User response with list of roles.
 */
@ApiModel
public class UserResponse {

    @ApiModelProperty(notes = "User's id")
    private Integer id;

    @ApiModelProperty(notes = "User's username")
    private String username;

    @ApiModelProperty(notes = "User's first name")
    private String firstName;

    @ApiModelProperty(notes = "User's last name")
    private String lastName;

    @ApiModelProperty(notes = "User's roles")
    private Set<String> roles;

    public UserResponse(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.roles = new HashSet<>();
        user.getRoles().forEach(userRole -> this.roles.add(userRole.getRole()));
    }

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Set<String> getRoles() {
        return roles;
    }
}
