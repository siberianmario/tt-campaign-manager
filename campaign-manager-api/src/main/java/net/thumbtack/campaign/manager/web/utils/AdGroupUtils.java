package net.thumbtack.campaign.manager.web.utils;

import net.thumbtack.campaign.manager.domain.model.AdGroup;
import net.thumbtack.campaign.manager.domain.model.ScheduleDay;
import net.thumbtack.campaign.manager.web.form.AdGroupCreateForm;
import net.thumbtack.campaign.manager.web.form.AdGroupUpdateForm;

import java.util.ArrayList;
import java.util.List;

/**
 * AdGroup Utils.
 */
public class AdGroupUtils {

    /**
     * Build AdGroup object with CreateForm parameters.
     * @param form CreateForm with parameters
     * @return new AdGroup
     */
    public static AdGroup buildAdGroup(AdGroupCreateForm form) {
        ValidationUtils.validateAdGroupCreateForm(form);
        return new AdGroup(null, form.getName(), form.getDescription());
    }

    /**
     * Update AdGroup with non-null UpdateForm parameters.
     * @param adGroup AdGroup to update
     * @param form UpdateForm with parameters
     * @return updated AdGroup
     */
    public static AdGroup updateAdGroup(AdGroup adGroup, AdGroupUpdateForm form) {
        ValidationUtils.validateAdGroupUpdateForm(form);
        if (form.getName() != null) {
            adGroup.setName(form.getName().orElse(null));
        }
        if (form.getDescription() != null) {
            adGroup.setDescription(form.getDescription().orElse(null));
        }
        if (form.getBrowsers() != null) {
            adGroup.setBrowsers(form.getBrowsers().orElse(null));
        }
        if (form.getOperatingSystems() != null) {
            adGroup.setOperatingSystems(form.getOperatingSystems().orElse(null));
        }
        if (form.getDevices() != null) {
            adGroup.setDevices(form.getDevices().orElse(null));
        }
        if (form.getSchedule() != null) {
            adGroup.getSchedule().forEach(scheduleDay -> scheduleDay.getHours().clear());
            adGroup.getSchedule().clear();

            List<ScheduleDay> newSchedule = form.getSchedule().orElse(new ArrayList<>());
            newSchedule.forEach(scheduleDay -> {
                scheduleDay.setAdGroup(adGroup);
                scheduleDay.getHours().forEach(scheduleHours -> scheduleHours.setScheduleDay(scheduleDay));
                adGroup.getSchedule().add(scheduleDay);
            });
        }
        return adGroup;
    }
}
