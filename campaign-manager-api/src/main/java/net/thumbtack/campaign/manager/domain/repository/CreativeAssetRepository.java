package net.thumbtack.campaign.manager.domain.repository;

import net.thumbtack.campaign.manager.domain.model.CreativeAsset;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository interface to access CreativeAsset data.
 */
public interface CreativeAssetRepository extends CrudRepository<CreativeAsset, Integer> {
}
