package net.thumbtack.campaign.manager.web.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.thumbtack.campaign.manager.domain.model.AdGroup;
import net.thumbtack.campaign.manager.domain.model.Creative;
import net.thumbtack.campaign.manager.domain.model.CreativeType;
import net.thumbtack.campaign.manager.domain.repository.AdGroupRepository;
import net.thumbtack.campaign.manager.domain.repository.CreativeAssetRepository;
import net.thumbtack.campaign.manager.domain.repository.CreativeRepository;
import net.thumbtack.campaign.manager.domain.repository.CreativeTypeRepository;
import net.thumbtack.campaign.manager.web.exceptions.NotFoundException;
import net.thumbtack.campaign.manager.web.form.CreativeCreateForm;
import net.thumbtack.campaign.manager.web.form.CreativeUpdateForm;
import net.thumbtack.campaign.manager.web.utils.CreativeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Creative Controller.
 */
@RestController
@RequestMapping("/v1")
@Transactional
public class CreativeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreativeController.class);

    @Autowired
    AdGroupRepository adGroupRepository;

    @Autowired
    CreativeRepository creativeRepository;

    @Autowired
    CreativeAssetRepository creativeAssetRepository;

    @Autowired
    CreativeTypeRepository creativeTypeRepository;

    /**
     * Get page of all AdGroup's Creative.
     * @return Page of Creative
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @ApiOperation(value = "getAll", nickname = "getAllCreatives")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "adGroupId", value = "AdGroup's id", required = true,
                    dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page."),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
                    value = "Sorting criteria in the format: property(,asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET, path = "/adgroups/{adGroupId}/creatives",
            produces = "application/json")
    public Page<Creative> getAll(@PathVariable final Integer adGroupId, @ApiIgnore final Pageable pageable) {
        AdGroup adGroup = adGroupRepository.findOne(adGroupId);
        if (adGroup == null) {
            throw new NotFoundException("AdGroup not found");
        }
        return creativeRepository.findByAdGroupId(adGroupId, pageable);
    }

    /**
     * Get Creative by id.
     * @return Creative with specified id
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @ApiOperation(value = "getById", nickname = "getCreativeById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "creativeId", value = "Creative's id", required = true, dataType = "int",
                    paramType = "path")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET, path = "/creatives/{creativeId}",
            produces = "application/json")
    public Creative getById(@PathVariable final Integer creativeId) {
        Creative creative = creativeRepository.findOne(creativeId);
        if (creative == null) {
            throw new NotFoundException("Creative not found");
        }
        return creative;
    }

    /**
     * Get Creative content by id.
     * @return Creative content
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @ApiOperation(value = "getContentById", nickname = "getCreativeContentById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "creativeId", value = "Creative's id", required = true, dataType = "int",
                    paramType = "path")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET, path = "/creatives/{creativeId}/content",
            produces = {"image/*", "application/json"})
    public byte[] getContentById(@PathVariable final Integer creativeId) {
        Creative creative = creativeRepository.findOne(creativeId);
        if (creative == null) {
            throw new NotFoundException("Creative not found");
        }
        return creative.getAsset().getBody();
    }

    /**
     * Find AdGroup's Creative by name.
     * @return AdGroup's Creative with specified name
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @ApiOperation(value = "findByName", nickname = "findCreativeByName")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "adGroupId", value = "AdGroup's id", required = true,
                    dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "name", value = "Creatives's name", required = true,
                    dataType = "string", paramType = "query")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET, path = "/adgroups/{adGroupId}/creatives/search",
            produces = "application/json")
    public Iterable<Creative> getByName(@PathVariable final Integer adGroupId,
                              @RequestParam final String name, @ApiIgnore final Pageable pageable) {
        AdGroup adGroup = adGroupRepository.findOne(adGroupId);
        if (adGroup == null) {
            throw new NotFoundException("AdGroup not found");
        }
        return creativeRepository.findByAdGroupIdAndNameIgnoreCaseContaining(adGroupId, name, pageable);
    }

    /**
     * Create new Creative.
     * @return created Creative
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "create", nickname = "createCreative")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "adGroupId", value = "AdGroup's id", required = true, dataType = "int",
                    paramType = "path"),
            @ApiImplicitParam(name = "form", value = "CreativeCreate form", required = true,
                    dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "file", value = "Creative's content", required = true,
                    dataType = "file", paramType = "form")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.POST, path = "/adgroups/{adGroupId}/creatives",
            consumes = "multipart/form-data", produces = "application/json")
    public Creative create(@PathVariable("adGroupId") final Integer adGroupId,
                            @RequestPart("form") final String form,
                            @RequestPart("file") final MultipartFile file) {
        Gson gson = new GsonBuilder().create();
        CreativeCreateForm creativeForm = gson.fromJson(form, CreativeCreateForm.class);
        AdGroup adGroup = adGroupRepository.findOne(adGroupId);
        if (adGroup == null) {
            throw new NotFoundException("Advertiser not found");
        }
        Creative creative = CreativeUtils.buildCreative(creativeForm, file);
        creative.setAdGroup(adGroup);
        CreativeType type = creativeTypeRepository.findByType(creativeForm.getType());
        creative.setType(type);
        creativeRepository.save(creative);
        return creative;
    }

    /**
     * Update Creative.
     * @return updated Creative
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "update", nickname = "updateCreative")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "creativeId", value = "Creative's id", required = true, dataType = "int",
                    paramType = "path")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.PUT, path = "/creatives/{creativeId}",
            consumes = "application/json", produces = "application/json")
    public Creative updateById(@PathVariable("creativeId") final Integer creativeId,
                            @RequestBody final CreativeUpdateForm form) {
        Creative creative = creativeRepository.findOne(creativeId);
        if (creative == null) {
            throw new NotFoundException("Creative not found");
        }
        creative = CreativeUtils.updateCreative(creative, form);
        if (form.getType() != null) {
            CreativeType type = creativeTypeRepository.findByType(form.getType());
            creative.setType(type);
        }
        creativeRepository.save(creative);
        return creative;
    }

    /**
     * Update Creative Content.
     * @return updated Creative
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "updateContent", nickname = "updateCreativeContent")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "creativeId", value = "Creative's id", required = true, dataType = "int",
                    paramType = "path"),
            @ApiImplicitParam(name = "file", value = "Creative's content", required = true,
                    dataType = "file", paramType = "form")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.POST, path = "/creatives/{creativeId}/content",
            produces = "application/json")
    public Creative updateContentById(@PathVariable("creativeId") final Integer creativeId,
                            @RequestParam("file") final MultipartFile file) {
        Creative creative = creativeRepository.findOne(creativeId);
        if (creative == null) {
            throw new NotFoundException("Creative not found");
        }
        creative = CreativeUtils.updateCreativeContent(creative, file);
        creativeRepository.save(creative);
        return creative;
    }

    /**
     * Delete Creative by id.
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "deleteById", nickname = "deleteCreativeById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "creativeId", value = "Creative's id", required = true, dataType = "int",
                    paramType = "path")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.DELETE, path = "/creatives/{creativeId}")
    public void deleteById(@PathVariable final Integer creativeId) {
        Creative creative = creativeRepository.findOne(creativeId);
        if (creative == null) {
            throw new NotFoundException("Creative not found");
        }
        creativeRepository.delete(creativeId);
        LOGGER.info("Deleted: {}", creative);
    }
}
