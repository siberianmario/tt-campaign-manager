package net.thumbtack.campaign.manager.web.form;

import net.thumbtack.campaign.manager.domain.model.TrackerTypeEnum;

import java.util.Optional;

/**
 * Form for updating Tracker.
 */
public class TrackerUpdateForm {
    Optional<String> body;
    TrackerTypeEnum type;

    public TrackerUpdateForm() {
    }

    public TrackerUpdateForm(Optional<String> body, TrackerTypeEnum type) {
        this.body = body;
        this.type = type;
    }

    public Optional<String> getBody() {
        return body;
    }

    public TrackerTypeEnum getType() {
        return type;
    }

    @Override
    public String toString() {
        return "TrackerUpdateForm{" +
                "body=" + body +
                ", type=" + type +
                '}';
    }
}
