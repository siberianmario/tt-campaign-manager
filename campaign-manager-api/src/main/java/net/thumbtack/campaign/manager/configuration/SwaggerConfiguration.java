package net.thumbtack.campaign.manager.configuration;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.annotations.ApiIgnore;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static springfox.documentation.schema.AlternateTypeRules.newRule;
import static springfox.documentation.service.ApiInfo.DEFAULT_CONTACT;

/**
 * Configures Swagger.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Autowired
    private volatile TypeResolver typeResolver;

    /**
     * @return bean that configures Swagger.
     */
    @Bean
    public Docket apiDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
            .apiInfo(new ApiInfo(
                    "Campaign Manager API",
                    "AdTech service to run ad campaigns",
                    "v1",
                    null,
                    DEFAULT_CONTACT,
                    null,
                    null
            ))
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.ant("/v1/**"))
            .build()
            .pathMapping("/")
            .directModelSubstitute(LocalDate.class, Date.class)
            .directModelSubstitute(ZonedDateTime.class, Date.class)
            .directModelSubstitute(LocalDateTime.class, Date.class)
            .directModelSubstitute(Timestamp.class, Date.class)
            .genericModelSubstitutes(ResponseEntity.class)
            .alternateTypeRules(
                newRule(
                    typeResolver.resolve(DeferredResult.class,
                    typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                    typeResolver.resolve(WildcardType.class)),
                newRule(
                    typeResolver.resolve(Optional.class,
                    typeResolver.resolve(WildcardType.class)),
                    typeResolver.resolve(WildcardType.class)
                ))
            .useDefaultResponseMessages(false)
            .globalResponseMessage(RequestMethod.GET,
                newArrayList(
                    new ResponseMessageBuilder()
                        .code(500)
                        .message("Internal server error")
                        .build()))
            .enableUrlTemplating(false)
            .ignoredParameterTypes(ApiIgnore.class);
    }

    /**
     * @return bean that configures UI.
     */
    @Bean
    public UiConfiguration uiConfig() {
        // No validator
        return new UiConfiguration(null);
    }
}
