package net.thumbtack.campaign.manager.domain.repository;

import net.thumbtack.campaign.manager.domain.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Repository interface to access User data.
 */
public interface UserRepository extends PagingAndSortingRepository<User, Integer> {

    /**
     * Find User by it's username.
     * @param username Username of the advertiser
     * @return Advertiser with specified name
     */
    User findByUsername(String username);
}
