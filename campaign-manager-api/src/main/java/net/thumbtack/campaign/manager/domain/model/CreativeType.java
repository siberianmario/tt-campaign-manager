package net.thumbtack.campaign.manager.domain.model;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * A top level entity for CreativeType.
 */
@Entity
@Table(name = "creative_type")
public class CreativeType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The id of the CreativeType", required = true)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "The type of the CreativeType", required = true)
    private CreativeTypeEnum type;

    public CreativeType() {
    }

    public CreativeType(CreativeTypeEnum type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CreativeTypeEnum getType() {
        return type;
    }

    public void setType(CreativeTypeEnum type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "CreativeType{" +
                "id=" + id +
                ", type=" + type +
                '}';
    }
}
