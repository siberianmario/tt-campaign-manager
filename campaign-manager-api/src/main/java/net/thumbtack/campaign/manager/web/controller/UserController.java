package net.thumbtack.campaign.manager.web.controller;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.thumbtack.campaign.manager.domain.model.User;
import net.thumbtack.campaign.manager.domain.model.UserRole;
import net.thumbtack.campaign.manager.domain.repository.UserRepository;
import net.thumbtack.campaign.manager.domain.repository.UserRoleRepository;
import net.thumbtack.campaign.manager.web.exceptions.NotFoundException;
import net.thumbtack.campaign.manager.web.form.UserCreateForm;
import net.thumbtack.campaign.manager.web.form.UserRoleUpdateForm;
import net.thumbtack.campaign.manager.web.form.UserUpdateForm;
import net.thumbtack.campaign.manager.web.responses.UserResponse;
import net.thumbtack.campaign.manager.web.utils.UserUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * API endpoints for registration and role management.
 */
@RestController
@RequestMapping("/v1/users")
@Transactional
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserRoleRepository userRoleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    /**
     * Get list of all Users.
     * @return List of Users
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @ApiOperation(value = "getAll", nickname = "getAllUsers")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page."),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
                    value = "Sorting criteria in the format: property(,asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success",
                    response = UserResponse.class, responseContainer = "List"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public Iterable<UserResponse> getAll(@ApiIgnore final Pageable pageable) {
        Iterable<User> users = userRepository.findAll(pageable);
        List<UserResponse> usersResponse = new ArrayList<>();
        users.forEach(user -> usersResponse.add(new UserResponse(user)));
        return new PageImpl<>(usersResponse, pageable, userRepository.count());
    }

    /**
     * Get User by id.
     * @return User with specified id
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @ApiOperation(value = "getById", nickname = "getUserById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "User's id", required = true, dataType = "int",
                    paramType = "path")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = UserResponse.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET, path = "/{id}", produces = "application/json")
    public UserResponse getById(@PathVariable final Integer id) {
        User user = userRepository.findOne(id);
        if (user == null) {
            throw new NotFoundException("User not found");
        }
        return new UserResponse(user);
    }

    /**
     * Find User by username.
     * @return User with specified username
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @ApiOperation(value = "findById", nickname = "findUserByUsername")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "User's username", required = true,
                    dataType = "string", paramType = "query")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = UserResponse.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET, path = "/search", produces = "application/json")
    public UserResponse getByUsername(@RequestParam final String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new NotFoundException("User not found");
        }
        return new UserResponse(user);
    }

    /**
     * Register new User.
     * @return registered User
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(value = "register", nickname = "registerUser")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = UserResponse.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.POST,
            produces = "application/json", consumes = "application/json")
    public UserResponse create(@RequestBody final UserCreateForm userForm) {
        LOGGER.info("Received: {}", userForm);
        User user = UserUtils.buildUser(userForm);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        userRoleRepository.save(user.getRoles());
        LOGGER.info("Created: {}", user);
        return new UserResponse(user);
    }

    /**
     * Update User by id.
     * @return updated User
     */
    @PreAuthorize("hasRole('ROLE_ADMIN') or principal.id == #id")
    @ApiOperation(value = "updateById", nickname = "updateUserById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "User's id", required = true, dataType = "int",
                    paramType = "path")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = UserResponse.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.PUT, path = "/{id}",
            produces = "application/json", consumes = "application/json")
    public UserResponse updateById(@RequestBody final UserUpdateForm userForm, @PathVariable final Integer id) {
        User user = userRepository.findOne(id);
        if (user == null) {
            throw new NotFoundException("User not found");
        }
        user = UserUtils.updateUser(user, userForm);
        if (userForm.getPassword() != null) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        userRepository.save(user);
        LOGGER.info("Update {} with {}", user, userForm);
        return new UserResponse(user);
    }

    /**
     * Update User roles by userId.
     * @return updated User
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(value = "updateRolesById", nickname = "updateUserRolesById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "User's id", required = true, dataType = "int",
                    paramType = "path")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = UserResponse.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.PUT, path = "/{id}/roles",
            produces = "application/json", consumes = "application/json")
    public UserResponse updateRolesById(@RequestBody final UserRoleUpdateForm roleForm,
                                        @PathVariable final Integer id) {
        User user = userRepository.findOne(id);
        if (user == null) {
            throw new NotFoundException("User not found");
        }
        LOGGER.info("Updating {} with {}", user, roleForm);

        Set<UserRole> oldRoles = user.getRoles();
        Set<UserRole> newRoles = UserUtils.buildUserRoles(user, roleForm);

        Set<UserRole> rolesToDelete = new HashSet<>(oldRoles);
        rolesToDelete.removeAll(newRoles);

        Set<UserRole> rolesToAdd = new HashSet<>(newRoles);
        rolesToAdd.removeAll(oldRoles);

        user.getRoles().removeAll(rolesToDelete);
        user.getRoles().addAll(rolesToAdd);
        return new UserResponse(user);
    }


    /**
     * Delete User by id.
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(value = "deleteById", nickname = "deleteUserById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "User's id", required = true, dataType = "int",
                    paramType = "path")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.DELETE, path = "/{id}")
    public void deleteById(@PathVariable final Integer id) {
        userRepository.delete(id);
        LOGGER.info("Delete User with id = {}", id);
    }
}
