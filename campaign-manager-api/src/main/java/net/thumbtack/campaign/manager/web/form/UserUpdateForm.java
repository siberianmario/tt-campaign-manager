package net.thumbtack.campaign.manager.web.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Optional;

/**
 * Form for updating User.
 */
@ApiModel
public class UserUpdateForm {

    @ApiModelProperty(notes = "User's password")
    private Optional<String> password;

    @ApiModelProperty(notes = "User's firstName")
    private Optional<String> firstName;

    @ApiModelProperty(notes = "User's lastName")
    private Optional<String> lastName;

    public UserUpdateForm() {
    }

    public Optional<String> getPassword() {
        return password;
    }

    public Optional<String> getFirstName() {
        return firstName;
    }

    public Optional<String> getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return "UserUpdateForm{" +
                "password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
