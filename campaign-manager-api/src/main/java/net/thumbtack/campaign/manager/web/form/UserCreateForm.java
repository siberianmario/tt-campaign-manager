package net.thumbtack.campaign.manager.web.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Set;

/**
 * Form for creating User.
 */
@ApiModel
public class UserCreateForm {

    @ApiModelProperty(notes = "User's username", required = true)
    private String username;

    @ApiModelProperty(notes = "User's password", required = true)
    private String password;

    @ApiModelProperty(notes = "User's firstName", required = true)
    private String firstName;

    @ApiModelProperty(notes = "User's lastName", required = true)
    private String lastName;

    @ApiModelProperty(notes = "User's roles")
    private Set<String> roles;

    public UserCreateForm() {
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Set<String> getRoles() {
        return roles;
    }

    @Override
    public String toString() {
        return "UserCreateForm{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", roles=" + roles +
                '}';
    }
}
