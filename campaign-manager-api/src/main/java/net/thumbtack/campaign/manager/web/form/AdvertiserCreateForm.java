package net.thumbtack.campaign.manager.web.form;

import io.swagger.annotations.ApiModelProperty;

/**
 * Form for creating Advertiser.
 */
public class AdvertiserCreateForm {

    @ApiModelProperty(notes = "The name of the Advertiser", required = true)
    private String name;

    @ApiModelProperty(notes = "The description of the Advertiser")
    private String description;

    @ApiModelProperty(notes = "The site of the Advertiser")
    private String site;

    protected AdvertiserCreateForm() {
    }

    public AdvertiserCreateForm(String name, String description, String site) {
        this.name = name;
        this.description = description;
        this.site = site;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getSite() {
        return site;
    }

    @Override
    public String toString() {
        return "AdvertiserCreateForm{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", site='" + site + '\'' +
                '}';
    }
}
