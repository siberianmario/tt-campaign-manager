package net.thumbtack.campaign.manager.domain.model;

/**
 * Creative Type Enum.
 */
public enum CreativeTypeEnum { IMAGE
}
