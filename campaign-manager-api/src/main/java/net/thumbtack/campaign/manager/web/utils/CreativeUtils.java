package net.thumbtack.campaign.manager.web.utils;

import net.thumbtack.campaign.manager.domain.model.Creative;
import net.thumbtack.campaign.manager.domain.model.CreativeAsset;
import net.thumbtack.campaign.manager.web.exceptions.ValidationException;
import net.thumbtack.campaign.manager.web.form.CreativeCreateForm;
import net.thumbtack.campaign.manager.web.form.CreativeUpdateForm;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

import static net.thumbtack.campaign.manager.web.utils.ValidationUtils.validateCreativeCreateForm;

/**
 * Creative Utils.
 */
public class CreativeUtils {

    /**
     * Build Creative object with CreateForm parameters.
     * @param form CreateForm with parameters
     * @param file File with actual Creative content
     * @return new Creative
     */
    public static Creative buildCreative(CreativeCreateForm form, MultipartFile file) {
        validateCreativeCreateForm(form);

        CreativeAsset asset = new CreativeAsset();
        Creative creative = new Creative(form.getName(), form.getDescription(), form.getLandingPageUrl(), asset);

        creative = updateCreativeContent(creative, file);

        return creative;
    }

    /**
     * Update Creative with UpdateForm parameters.
     * @param creative Creative to update
     * @param form UpdateForm with parameters
     * @return updated Creative
     */
    public static Creative updateCreative(Creative creative, CreativeUpdateForm form) {
        ValidationUtils.validateCreativeUpdateForm(form);
        if (form.getName() != null) {
            creative.setName(form.getName().get());
        }
        if (form.getDescription() != null) {
            creative.setDescription(form.getDescription().orElse(null));
        }
        if (form.getLandingPageUrl() != null) {
            creative.setLandingPageUrl(form.getLandingPageUrl().get());
        }
        return creative;
    }

    /**
     * Update Creative content with received file.
     * @param creative Creative to update
     * @param file Content file received
     * @return updated Creative
     */
    public static Creative updateCreativeContent(Creative creative, MultipartFile file) {

        if (file == null || file.isEmpty()) {
            throw new ValidationException("Provided file is empty");
        } else {
            byte[] assetBody;
            int width;
            int height;
            try {
                BufferedImage img = ImageIO.read(file.getInputStream());
                if (img == null) {
                    throw new ValidationException("Invalid image file");
                }
                width = img.getWidth();
                height = img.getHeight();
                assetBody = file.getBytes();
            } catch (IOException e) {
                e.printStackTrace();
                throw new ValidationException("Invalid resource file");
            }

            String fileName = file.getOriginalFilename();

            creative.setHeight(height);
            creative.setWidth(width);
            creative.getAsset().setFileName(FilenameUtils.getBaseName(fileName));
            creative.getAsset().setExtension(FilenameUtils.getExtension(fileName));
            creative.getAsset().setBody(assetBody);
            return creative;
        }
    }
}
