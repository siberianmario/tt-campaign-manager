package net.thumbtack.campaign.manager.web.controller;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.thumbtack.campaign.manager.domain.model.AdGroup;
import net.thumbtack.campaign.manager.domain.model.Campaign;
import net.thumbtack.campaign.manager.domain.repository.AdGroupRepository;
import net.thumbtack.campaign.manager.domain.repository.AdvertiserRepository;
import net.thumbtack.campaign.manager.domain.repository.CampaignRepository;
import net.thumbtack.campaign.manager.domain.repository.CampaignStatusRepository;
import net.thumbtack.campaign.manager.web.exceptions.NotFoundException;
import net.thumbtack.campaign.manager.web.form.AdGroupCreateForm;
import net.thumbtack.campaign.manager.web.form.AdGroupUpdateForm;
import net.thumbtack.campaign.manager.web.utils.AdGroupUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * AdGroup Controller.
 */
@RestController
@RequestMapping("/v1")
@Transactional
public class AdGroupController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdGroupController.class);

    @Autowired
    AdvertiserRepository advertiserRepository;

    @Autowired
    CampaignRepository campaignRepository;

    @Autowired
    CampaignStatusRepository campaignStatusRepository;

    @Autowired
    AdGroupRepository adGroupRepository;

    /**
     * Get page of all Campaign's AdGroups.
     * @return Page of AdGroups
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @ApiOperation(value = "getAll", nickname = "getAllAdGroups")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "campaignId", value = "Campaign's id", required = true,
                    dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page."),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
                    value = "Sorting criteria in the format: property(,asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = AdGroup.class, responseContainer = "List"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET, path = "/campaigns/{campaignId}/adgroups",
            produces = "application/json")
    public Iterable<AdGroup> getAll(@PathVariable final Integer campaignId, @ApiIgnore final Pageable pageable) {
        Campaign campaign = campaignRepository.findOne(campaignId);
        if (campaign == null) {
            throw new NotFoundException("Campaign not found");
        }
        return adGroupRepository.findByCampaignId(campaignId, pageable);
    }

    /**
     * Get AdGroup by id.
     * @return AdGroup with specified id
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @ApiOperation(value = "getById", nickname = "getAdGroupById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "adGroupId", value = "AdGroup's id", required = true, dataType = "int",
                    paramType = "path")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = AdGroup.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET, path = "/adgroups/{adGroupId}",
            produces = "application/json")
    public AdGroup getById(@PathVariable final Integer adGroupId) {
        AdGroup adGroup = adGroupRepository.findOne(adGroupId);
        if (adGroup == null) {
            throw new NotFoundException("AdGroup not found");
        }
        return adGroup;
    }

    /**
     * Find Campaign's AdGroup by name.
     * @return Campaign's AdGroup with specified name
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @ApiOperation(value = "findByName", nickname = "findAdGroupByName")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "campaignId", value = "Campaign's id", required = true,
                    dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "name", value = "AdGroup's name", required = true,
                    dataType = "string", paramType = "query")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = AdGroup.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET, path = "/campaigns/{campaignId}/adgroups/search",
            produces = "application/json")
    public Iterable<AdGroup> getByName(@PathVariable final Integer campaignId,
                             @RequestParam final String name, @ApiIgnore final Pageable pageable) {
        Campaign campaign = campaignRepository.findOne(campaignId);
        if (campaign == null) {
            throw new NotFoundException("Campaign not found");
        }
        return adGroupRepository.findByCampaignIdAndNameIgnoreCaseContaining(campaignId, name, pageable);
    }

    /**
     * Create new AdGroup.
     * @return created AdGroup
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "create", nickname = "createAdGroup")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "campaignId", value = "Campaign's id", required = true, dataType = "int",
                    paramType = "path")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = AdGroup.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.POST, path = "/campaigns/{campaignId}/adgroups",
            produces = "application/json", consumes = "application/json")
    public AdGroup create(@PathVariable final Integer campaignId, @RequestBody final AdGroupCreateForm form) {
        Campaign campaign = campaignRepository.findOne(campaignId);
        if (campaign == null) {
            throw new NotFoundException("Campaign not found");
        }
        AdGroup adGroup = AdGroupUtils.buildAdGroup(form);
        adGroup.setCampaign(campaign);
        adGroupRepository.save(adGroup);
        LOGGER.info("Created: {}", adGroup);
        return adGroup;
    }

    /**
     * Update AdGroup by id.
     * @return updated AdGroup
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "updateById", nickname = "updateAdGroupById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "adGroupId", value = "AdGroup's id", required = true, dataType = "int",
                    paramType = "path")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = AdGroup.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.PUT, path = "/adgroups/{adGroupId}",
            produces = "application/json", consumes = "application/json")
    public AdGroup updateById(@RequestBody final AdGroupUpdateForm form, @PathVariable final Integer adGroupId) {
        AdGroup adGroup = adGroupRepository.findOne(adGroupId);
        if (adGroup == null) {
            throw new NotFoundException("AdGroup not found");
        }
        LOGGER.info("Trying to update {} with {}", adGroup, form);
        try {
            adGroup = AdGroupUtils.updateAdGroup(adGroup, form);
            adGroupRepository.save(adGroup);
        } catch (RuntimeException ex) {
            LOGGER.info("Update failed: ", ex.getMessage());
            throw ex;
        }
        LOGGER.info("Updated successfully: {}", adGroup);
        return adGroup;
    }

    /**
     * Delete AdGroup by id.
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "deleteById", nickname = "deleteAdGroupById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "adGroupId", value = "AdGroup's id", required = true, dataType = "int",
                    paramType = "path")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.DELETE, path = "/adgroups/{adGroupId}")
    public void deleteById(@PathVariable final Integer adGroupId) {
        AdGroup adGroup = adGroupRepository.findOne(adGroupId);
        if (adGroup == null) {
            throw new NotFoundException("AdGroup not found");
        }
        adGroupRepository.delete(adGroupId);
        LOGGER.info("Deleted: {}", adGroup);
    }
}
