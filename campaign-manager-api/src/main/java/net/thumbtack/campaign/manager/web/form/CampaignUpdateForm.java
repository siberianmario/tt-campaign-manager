package net.thumbtack.campaign.manager.web.form;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Optional;

/**
 * Form for updating Campaign.
 */
public class CampaignUpdateForm {

    @ApiModelProperty(notes = "The name of the Campaign", required = true, example = "name")
    private Optional<String> name;

    @ApiModelProperty(notes = "The description of the Campaign", example = "description")
    private Optional<String> description;

    @ApiModelProperty(notes = "The start date of the Campaign",
            required = true, example = "1970-01-01T00:00:00+00:00")
    private Optional<Timestamp> startDate;

    @ApiModelProperty(notes = "The end date of the Campaign",
            required = true, example = "1970-01-01T00:00:00+00:00")
    private Optional<Timestamp> endDate;

    @ApiModelProperty(notes = "The lifetime impression capacity of the Campaign",
            required = true, example = "1000")
    private Optional<Integer> lifetimeImpCap;

    @ApiModelProperty(notes = "The lifetime budget of the Campaign",
            required = true, example = "100")
    private Optional<BigDecimal> lifetimeBudget;

    protected CampaignUpdateForm() {
    }

    public CampaignUpdateForm(Optional<String> name, Optional<String> description,
                              Optional<Timestamp> startDate, Optional<Timestamp> endDate,
                              Optional<Integer> lifetimeImpCap, Optional<BigDecimal> lifetimeBudget) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.lifetimeImpCap = lifetimeImpCap;
        this.lifetimeBudget = lifetimeBudget;
    }

    public Optional<String> getName() {
        return name;
    }

    public Optional<String> getDescription() {
        return description;
    }

    public Optional<Timestamp> getStartDate() {
        return startDate;
    }

    public Optional<Timestamp> getEndDate() {
        return endDate;
    }

    public Optional<Integer> getLifetimeImpCap() {
        return lifetimeImpCap;
    }

    public Optional<BigDecimal> getLifetimeBudget() {
        return lifetimeBudget;
    }

    @Override
    public String toString() {
        return "CampaignUpdateForm{" +
                "name=" + name +
                ", description=" + description +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", lifetimeImpCap=" + lifetimeImpCap +
                ", lifetimeBudget=" + lifetimeBudget +
                '}';
    }
}
