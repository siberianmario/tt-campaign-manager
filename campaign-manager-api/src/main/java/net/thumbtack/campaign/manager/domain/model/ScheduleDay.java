package net.thumbtack.campaign.manager.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

/**
 * Days of week for AdGroup scheduling.
 */
@Entity
@Table(name = "schedule_day")
public class ScheduleDay {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "ad_group_id")
    private AdGroup adGroup;

    @Enumerated(EnumType.STRING)
    @Column(name = "day_of_week")
    private DayOfWeek dayOfWeek;

    @OneToMany(mappedBy = "scheduleDay", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ScheduleHours> hours = new ArrayList<>();

    public ScheduleDay() {
    }

    public ScheduleDay(AdGroup adGroup, DayOfWeek dayOfWeek) {
        this.adGroup = adGroup;
        this.dayOfWeek = dayOfWeek;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AdGroup getAdGroup() {
        return adGroup;
    }

    public void setAdGroup(AdGroup adGroup) {
        this.adGroup = adGroup;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public List<ScheduleHours> getHours() {
        return hours;
    }

    public void setHours(List<ScheduleHours> hours) {
        this.hours = hours;
    }

    @Override
    public String toString() {
        return "ScheduleDay{" +
                "dayOfWeek=" + dayOfWeek +
                ", hours=" + hours +
                '}';
    }
}
