package net.thumbtack.campaign.manager.web.form;

import io.swagger.annotations.ApiModelProperty;

/**
 * Form for creating AdGroup.
 */
public class AdGroupCreateForm {

    @ApiModelProperty(notes = "The name of the AdGroup", required = true)
    private String name;

    @ApiModelProperty(notes = "The description of the AdGroup")
    private String description;

    public AdGroupCreateForm() {
    }

    public AdGroupCreateForm(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "AdGroupCreateForm{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
