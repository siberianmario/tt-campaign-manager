package net.thumbtack.campaign.manager.domain.repository;

import net.thumbtack.campaign.manager.domain.model.TrackerType;
import net.thumbtack.campaign.manager.domain.model.TrackerTypeEnum;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository to access TrackerType data.
 */
public interface TrackerTypeRepository extends CrudRepository<TrackerType, Integer> {

    /**
     * Find TrackerType by it's Type.
     * @param type Type of the TrackerType
     * @return TrackerType with specified Type
     */
    TrackerType findByType(TrackerTypeEnum type);
}
