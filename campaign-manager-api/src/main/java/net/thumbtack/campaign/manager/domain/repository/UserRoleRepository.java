package net.thumbtack.campaign.manager.domain.repository;

import net.thumbtack.campaign.manager.domain.model.UserRole;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

/**
 * Repository interface to access UserRole data.
 */
public interface UserRoleRepository extends CrudRepository<UserRole, Integer> {

    /**
     * Find UserRoles by user's username.
     * @param username Username of the user
     * @return UserRoles of the user
     */
    Collection<UserRole> findByUserUsername(String username);
}
