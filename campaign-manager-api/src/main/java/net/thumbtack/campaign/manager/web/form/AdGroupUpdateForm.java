package net.thumbtack.campaign.manager.web.form;

import io.swagger.annotations.ApiModelProperty;
import net.thumbtack.campaign.manager.domain.model.BrowserEnum;
import net.thumbtack.campaign.manager.domain.model.DeviceEnum;
import net.thumbtack.campaign.manager.domain.model.OperatingSystemEnum;
import net.thumbtack.campaign.manager.domain.model.ScheduleDay;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Form for updating AdGroup.
 */
public class AdGroupUpdateForm {

    @ApiModelProperty(notes = "The name of the AdGroup", required = true)
    private Optional<String> name;

    @ApiModelProperty(notes = "The description of the AdGroup")
    private Optional<String> description;

    @ApiModelProperty(notes = "Target browsers of the AdGroup")
    private Optional<Set<BrowserEnum>> browsers;

    @ApiModelProperty(notes = "Target operating systems of the AdGroup")
    private Optional<Set<OperatingSystemEnum>> operatingSystems;

    @ApiModelProperty(notes = "Target devices of the AdGroup")
    private Optional<Set<DeviceEnum>> devices;

    @ApiModelProperty(notes = "Schedule of the AdGroup")
    private Optional<List<ScheduleDay>> schedule;

    public AdGroupUpdateForm() {
    }

    public AdGroupUpdateForm(Optional<String> name, Optional<String> description, Optional<Set<BrowserEnum>> browsers,
                             Optional<Set<OperatingSystemEnum>> operatingSystems, Optional<Set<DeviceEnum>> devices,
                             Optional<List<ScheduleDay>> schedule) {
        this.name = name;
        this.description = description;
        this.browsers = browsers;
        this.operatingSystems = operatingSystems;
        this.devices = devices;
        this.schedule = schedule;
    }

    public Optional<String> getName() {
        return name;
    }

    public Optional<String> getDescription() {
        return description;
    }

    public Optional<Set<BrowserEnum>> getBrowsers() {
        return browsers;
    }

    public Optional<Set<OperatingSystemEnum>> getOperatingSystems() {
        return operatingSystems;
    }

    public Optional<Set<DeviceEnum>> getDevices() {
        return devices;
    }

    public Optional<List<ScheduleDay>> getSchedule() {
        return schedule;
    }

    @Override
    public String toString() {
        return "AdGroupUpdateForm{" +
                "name=" + name +
                ", description=" + description +
                ", browsers=" + browsers +
                ", operatingSystems=" + operatingSystems +
                ", devices=" + devices +
                ", schedule=" + schedule +
                '}';
    }
}
