package net.thumbtack.campaign.manager.web.utils;

import net.thumbtack.campaign.manager.domain.model.Campaign;
import net.thumbtack.campaign.manager.domain.model.CampaignStatusEnum;
import net.thumbtack.campaign.manager.web.exceptions.ValidationException;
import net.thumbtack.campaign.manager.web.form.CampaignCreateForm;
import net.thumbtack.campaign.manager.web.form.CampaignUpdateForm;

import java.util.Date;

/**
 * Campaign Utils.
 */
public class CampaignUtils {

    /**
     * Build Campaign object with CreateForm parameters.
     * @param form CreateForm with parameters
     * @return new Campaign
     */
    public static Campaign buildCampaign(CampaignCreateForm form) {
        ValidationUtils.validateCampaignCreateForm(form);
        return new Campaign(null, form.getName(), form.getDescription(), null, form.getStartDate(),
                form.getEndDate(), form.getLifetimeImpCap(), form.getLifetimeBudget());
    }

    /**
     * Update Campaign with non-null UpdateForm parameters.
     * @param campaign Campaign to update
     * @param form UpdateForm with parameters
     * @return updated Campaign
     */
    public static Campaign updateCampaign(Campaign campaign, CampaignUpdateForm form) {
        ValidationUtils.validateCampaignUpdateForm(form);
        if (form.getName() != null) {
            campaign.setName(form.getName().get());
        }
        if (form.getDescription() != null) {
            campaign.setDescription(form.getDescription().orElse(null));
        }
        if (form.getStartDate() != null) {
            campaign.setStartDate(form.getStartDate().get());
        }
        if (form.getEndDate() != null) {
            campaign.setEndDate(form.getEndDate().get());
        }
        if (campaign.getEndDate().before(campaign.getStartDate())) {
            throw new ValidationException("Campaign end date can't be earlier than start date");
        }
        if (form.getLifetimeImpCap() != null) {
            campaign.setLifetimeImpCap(form.getLifetimeImpCap().get());
        }
        if (form.getLifetimeBudget() != null) {
            campaign.setLifetimeBudget(form.getLifetimeBudget().get());
        }
        return campaign;
    }

    /**
     * Activate Campaign.
     * @param campaign Campaign to activate
     * @return new Campaign status
     */
    public static CampaignStatusEnum activate(Campaign campaign) {
        Date curDate = new Date();
        if (campaign.getStartDate().after(curDate)) {
            return CampaignStatusEnum.PENDING;
        } else if (campaign.getEndDate().after(curDate)) {
            return CampaignStatusEnum.ACTIVE;
        } else {
            return CampaignStatusEnum.ENDED;
        }
    }

    /**
     * Pause Campaign.
     * @param campaign Campaign to pause
     * @return new Campaign status
     */
    public static CampaignStatusEnum pause(Campaign campaign) {
        Date curDate = new Date();
        if (campaign.getStartDate().after(curDate)) {
            return CampaignStatusEnum.PAUSED;
        } else if (campaign.getEndDate().after(curDate)) {
            return CampaignStatusEnum.PAUSED;
        } else {
            return CampaignStatusEnum.ENDED;
        }
    }
}
