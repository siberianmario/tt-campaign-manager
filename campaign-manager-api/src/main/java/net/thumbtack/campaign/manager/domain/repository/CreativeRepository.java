package net.thumbtack.campaign.manager.domain.repository;

import net.thumbtack.campaign.manager.domain.model.Creative;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Collection;

/**
 * Repository interface to access Creative data.
 */
public interface CreativeRepository extends PagingAndSortingRepository<Creative, Integer> {

    /**
     * Find Creative by it's name adn AdGroup Id.
     * @param id AdGroup id
     * @param name Name of the Creative
     * @return Creative with specified name
     */
    Creative findByAdGroupIdAndName(Integer id, String name);

    /**
     * Find Creative by it's name adn AdGroup Id.
     * @param id AdGroup id
     * @param name Name of the Creative
     * @return Creative with specified name
     */
    Page<Creative> findByAdGroupIdAndNameIgnoreCaseContaining(Integer id, String name, Pageable pageable);

    /**
     * Find all Creatives by AdGroup Id.
     * @param id Id of the AdGroup
     * @return Collection of AdGroup's Creatives
     */
    Collection<Creative> findByAdGroupId(Integer id);

    /**
     * Find page of Creatives by AdGroup Id.
     * @param id Id of the AdGroup
     * @param pageable Page info
     * @return Collection of AdGroup's Creatives
     */
    Page<Creative> findByAdGroupId(Integer id, Pageable pageable);
}
