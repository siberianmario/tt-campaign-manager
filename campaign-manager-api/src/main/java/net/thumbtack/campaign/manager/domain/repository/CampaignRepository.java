package net.thumbtack.campaign.manager.domain.repository;

import net.thumbtack.campaign.manager.domain.model.Campaign;
import net.thumbtack.campaign.manager.domain.model.CampaignStatusEnum;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Collection;

/**
 * Repository interface to access Campaign data.
 */
public interface CampaignRepository extends PagingAndSortingRepository<Campaign, Integer> {

    /**
     * Find Campaign by it's name.
     * @param name Name of the Campaign
     * @return Campaign with specified name
     */
    Campaign findByName(String name);

    /**
     * Find Campaign by it's name and Advertiser id.
     * @param id Advertiser's id
     * @param name Name of the Campaign
     * @return Campaign with specified name
     */
    Campaign findByAdvertiserIdAndName(Integer id, String name);

    /**
     * Find Campaign by it's name and Advertiser id.
     * @param id Advertiser's id
     * @param name Name of the Campaign
     * @return Campaign with specified name
     */
    Page<Campaign> findByAdvertiserIdAndNameIgnoreCaseContaining(Integer id, String name, Pageable pageable);

    /**
     * Find all Campaigns by status.
     * @param status Status of the Campaign
     * @return Collection of Campaigns with specified status
     */
    Collection<Campaign> findByStatusStatus(CampaignStatusEnum status);

    /**
     * Find page of Campaigns by status.
     * @param status Status of the Campaign
     * @return Page of Campaigns with specified status
     */
    Page<Campaign> findByStatusStatus(CampaignStatusEnum status, Pageable pageable);

    /**
     * Find all Campaigns by Advertiser Id.
     * @param id Id of the Advertiser
     * @return Collection of Advertiser's Campaigns
     */
    Collection<Campaign> findByAdvertiserId(Integer id);

    /**
     * Find page of Campaigns by Advertiser Id.
     * @param id Id of the Advertiser
     * @return Page of Advertiser's Campaigns
     */
    Page<Campaign> findByAdvertiserId(Integer id, Pageable pageable);

    /**
     * Find all Campaigns by Advertiser Id.
     * @param id Id of the Advertiser
     * @param status Status of the Campaign
     * @return Collection of Advertiser's Campaigns
     */
    Collection<Campaign> findByAdvertiserIdAndStatusStatus(Integer id, CampaignStatusEnum status);

    /**
     * Find page of Campaigns by Advertiser Id.
     * @param id Id of the Advertiser
     * @param status Status of the Campaign
     * @return Page of Advertiser's Campaigns
     */
    Page<Campaign> findByAdvertiserIdAndStatusStatus(
            Integer id, CampaignStatusEnum status, Pageable pageable);
}
