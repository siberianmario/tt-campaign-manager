package net.thumbtack.campaign.manager.web.form;

import io.swagger.annotations.ApiModelProperty;
import net.thumbtack.campaign.manager.domain.model.CreativeTypeEnum;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * Form for creating Creative.
 */
public class CreativeCreateForm {

    @ApiModelProperty(notes = "The name of the Creative", required = true, example = "name")
    private String name;

    @ApiModelProperty(notes = "The description of the Creative", example = "description")
    private String description;

    @ApiModelProperty(notes = "The landing page url of the Creative", required = true)
    private String landingPageUrl;

    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "The type of the Creative", required = true)
    private CreativeTypeEnum type;

    public CreativeCreateForm() {
    }

    public CreativeCreateForm(String name, String description, String landingPageUrl, CreativeTypeEnum type) {
        this.name = name;
        this.description = description;
        this.landingPageUrl = landingPageUrl;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getLandingPageUrl() {
        return landingPageUrl;
    }

    public CreativeTypeEnum getType() {
        return type;
    }

    @Override
    public String toString() {
        return "CreativeCreateForm{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", landingPageUrl='" + landingPageUrl + '\'' +
                ", type=" + type +
                '}';
    }
}
