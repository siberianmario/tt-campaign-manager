package net.thumbtack.campaign.manager.domain.model;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * A top level entity for campaign status.
 */
@Entity
@Table(name = "campaign_status")
public class CampaignStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The id of the campaign status", required = true)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "The status of the the campaign", required = true)
    private CampaignStatusEnum status;

    public CampaignStatus() {
    }

    public CampaignStatus(CampaignStatusEnum status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CampaignStatusEnum getStatus() {
        return status;
    }

    public void setStatus(CampaignStatusEnum status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "CampaignStatus{" +
                "id=" + id +
                ", status=" + status +
                '}';
    }
}
