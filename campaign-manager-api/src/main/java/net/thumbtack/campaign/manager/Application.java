package net.thumbtack.campaign.manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * Sample spring  boot application.
 */
@SpringBootApplication
public class Application {

    /**
     * @param args command line arguments
     * @throws Exception Runtime exception
     */
    public static void main(String[] args) throws Exception {
        ApplicationContext ctx = SpringApplication.run(Application.class, args);
    }
}
