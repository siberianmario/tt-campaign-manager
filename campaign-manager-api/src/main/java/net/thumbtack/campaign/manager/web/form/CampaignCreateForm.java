package net.thumbtack.campaign.manager.web.form;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Form for creating Campaign.
 */
public class CampaignCreateForm {

    @ApiModelProperty(notes = "The name of the Campaign", required = true, example = "name")
    private String name;

    @ApiModelProperty(notes = "The description of the Campaign", example = "description")
    private String description;

    @ApiModelProperty(notes = "The start date of the Campaign",
            required = true, example = "1970-01-01T00:00:00+00:00")
    private Timestamp startDate;

    @ApiModelProperty(notes = "The end date of the Campaign",
            required = true, example = "1970-01-01T00:00:00+00:00")
    private Timestamp endDate;

    @ApiModelProperty(notes = "The lifetime impression capacity of the Campaign",
            required = true, example = "1000")
    private Integer lifetimeImpCap;

    @ApiModelProperty(notes = "The lifetime budget of the Campaign",
            required = true, example = "100")
    private BigDecimal lifetimeBudget;

    protected CampaignCreateForm() {
    }

    public CampaignCreateForm(String name, String description, Timestamp startDate,
                              Timestamp endDate, Integer lifetimeImpCap, BigDecimal lifetimeBudget) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.lifetimeImpCap = lifetimeImpCap;
        this.lifetimeBudget = lifetimeBudget;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public Integer getLifetimeImpCap() {
        return lifetimeImpCap;
    }

    public BigDecimal getLifetimeBudget() {
        return lifetimeBudget;
    }

    @Override
    public String toString() {
        return "CampaignCreateForm{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", lifetimeImpCap=" + lifetimeImpCap +
                ", lifetimeBudget=" + lifetimeBudget +
                '}';
    }
}
