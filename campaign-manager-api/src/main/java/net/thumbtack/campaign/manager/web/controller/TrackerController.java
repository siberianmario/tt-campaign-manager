package net.thumbtack.campaign.manager.web.controller;

import io.swagger.annotations.ApiOperation;
import net.thumbtack.campaign.manager.domain.model.Creative;
import net.thumbtack.campaign.manager.domain.model.Tracker;
import net.thumbtack.campaign.manager.domain.model.TrackerType;
import net.thumbtack.campaign.manager.domain.repository.CreativeRepository;
import net.thumbtack.campaign.manager.domain.repository.TrackerRepository;
import net.thumbtack.campaign.manager.domain.repository.TrackerTypeRepository;
import net.thumbtack.campaign.manager.web.exceptions.NotFoundException;
import net.thumbtack.campaign.manager.web.form.TrackerCreateForm;
import net.thumbtack.campaign.manager.web.form.TrackerUpdateForm;
import net.thumbtack.campaign.manager.web.utils.TrackerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Tracker Controller.
 */
@RestController
@RequestMapping("/v1")
@Transactional
public class TrackerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrackerController.class);

    @Autowired
    CreativeRepository creativeRepository;

    @Autowired
    TrackerRepository trackerRepository;

    @Autowired
    TrackerTypeRepository trackerTypeRepository;

    /**
     * Get all Creative's Trackers.
     * @return List of Campaigns
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @ApiOperation(value = "getAll", nickname = "getAllTrackers", response = Tracker.class, responseContainer = "List")
    @RequestMapping(method = RequestMethod.GET, path = "/creatives/{creativeId}/trackers",
            produces = "application/json")
    public Iterable<Tracker> getAll(@PathVariable final Integer creativeId) {
        Creative creative = creativeRepository.findOne(creativeId);
        if (creative == null) {
            throw new NotFoundException("Creative not found");
        }
        return trackerRepository.findByCreativeId(creativeId);
    }

    /**
     * Get Tracker by id.
     * @return Tracker with specified id
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @ApiOperation(value = "getById", nickname = "getTrackerById")
    @RequestMapping(method = RequestMethod.GET, path = "/trackers/{trackerId}",
            produces = "application/json")
    public Tracker getById(@PathVariable final Integer trackerId) {
        Tracker tracker = trackerRepository.findOne(trackerId);
        if (tracker == null) {
            throw new NotFoundException("Tracker not found");
        }
        return tracker;
    }

    /**
     * Create new Tracker.
     * @return created Tracker
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "create", nickname = "createTracker")
    @RequestMapping(method = RequestMethod.POST, path = "/creatives/{creativeId}/trackers",
            produces = "application/json", consumes = "application/json")
    public Tracker create(@PathVariable final Integer creativeId, @RequestBody TrackerCreateForm form) {
        Creative creative = creativeRepository.findOne(creativeId);
        if (creative == null) {
            throw new NotFoundException("Creative not found");
        }
        Tracker tracker = TrackerUtils.buildTracker(form);
        tracker.setCreative(creative);
        TrackerType type = trackerTypeRepository.findByType(form.getType());
        tracker.setTrackerType(type);
        trackerRepository.save(tracker);
        LOGGER.info("Created: {}", tracker);
        return tracker;
    }

    /**
     * Update Tracker.
     * @return updated Tracker
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "update", nickname = "updateTracker")
    @RequestMapping(method = RequestMethod.PUT, path = "/trackers/{trackerId}/",
            produces = "application/json", consumes = "application/json")
    public Tracker update(@PathVariable final Integer trackerId, @RequestBody final TrackerUpdateForm form) {
        final Tracker tracker = trackerRepository.findOne(trackerId);
        if (tracker == null) {
            throw new NotFoundException("Tracker not found");
        }
        LOGGER.info("Trying to update {} with {}", tracker, form);
        try {
            TrackerUtils.updateTracker(tracker, form);
            if (form.getType() != null) {
                TrackerType type = trackerTypeRepository.findByType(form.getType());
                tracker.setTrackerType(type);
            }
            trackerRepository.save(tracker);
            LOGGER.info("Updated successfully");
            return tracker;
        } catch (RuntimeException ex) {
            LOGGER.info("Update failed: ", ex.getMessage());
            throw ex;
        }
    }

    /**
     * Delete Tracker by id.
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "deleteById", nickname = "deleteTrackerById")
    @RequestMapping(method = RequestMethod.DELETE, path = "/trackers/{trackerId}")
    public void deleteById(@PathVariable final Integer trackerId) {
        Tracker tracker = trackerRepository.findOne(trackerId);
        if (tracker == null) {
            throw new NotFoundException("Tracker not found");
        }
        trackerRepository.delete(trackerId);
        LOGGER.info("Deleted: {}", tracker);
    }
}
