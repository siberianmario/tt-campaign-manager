package net.thumbtack.campaign.manager.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * A top level entity for campaign.
 */
@Entity
@Table(name = "campaign")
public class Campaign {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The id of the Campaign", required = true)
    private Integer id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "advertiser_id")
    private Advertiser advertiser;

    @ApiModelProperty(notes = "The name of the Campaign", required = true)
    private String name;

    @ApiModelProperty(notes = "The description of the Campaign")
    private String description;

    @OneToOne
    @JoinColumn(name = "status_id")
    @ApiModelProperty(notes = "The status of the Campaign", required = true)
    private CampaignStatus status;

    @Column(name = "start_date")
    @ApiModelProperty(notes = "The start date of the Campaign", required = true)
    private Timestamp startDate;

    @Column(name = "end_date")
    @ApiModelProperty(notes = "The end date of the Campaign", required = true)
    private Timestamp endDate;

    @Column(name = "lifetime_impression_cap")
    @ApiModelProperty(notes = "The lifetime impression capacity of the Campaign", required = true)
    private Integer lifetimeImpCap;

    @Column(name = "lifetime_budget")
    @ApiModelProperty(notes = "The lifetime budget of the Campaign", required = true)
    private BigDecimal lifetimeBudget;

    @JsonIgnore
    @OneToMany(mappedBy = "campaign", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<AdGroup> adGroups = new ArrayList<>();

    public Campaign() {
    }

    public Campaign(Advertiser advertiser, String name, String description, CampaignStatus status,
                    Timestamp startDate, Timestamp endDate, Integer lifetimeImpCap, BigDecimal lifetimeBudget) {
        this.advertiser = advertiser;
        this.name = name;
        this.description = description;
        this.status = status;
        this.startDate = startDate;
        this.endDate = endDate;
        this.lifetimeImpCap = lifetimeImpCap;
        this.lifetimeBudget = lifetimeBudget;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Advertiser getAdvertiser() {
        return advertiser;
    }

    public void setAdvertiser(Advertiser advertiser) {
        this.advertiser = advertiser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CampaignStatus getStatus() {
        return status;
    }

    public void setStatus(CampaignStatus status) {
        this.status = status;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public Integer getLifetimeImpCap() {
        return lifetimeImpCap;
    }

    public void setLifetimeImpCap(Integer lifetimeImpCap) {
        this.lifetimeImpCap = lifetimeImpCap;
    }

    public BigDecimal getLifetimeBudget() {
        return lifetimeBudget;
    }

    public void setLifetimeBudget(BigDecimal lifetimeBudget) {
        this.lifetimeBudget = lifetimeBudget;
    }

    public List<AdGroup> getAdGroups() {
        return adGroups;
    }

    public void setAdGroups(List<AdGroup> adGroups) {
        this.adGroups = adGroups;
    }

    @Override
    public String toString() {
        return "Campaign{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", lifetimeImpCap=" + lifetimeImpCap +
                ", lifetimeBudget=" + lifetimeBudget +
                '}';
    }
}
