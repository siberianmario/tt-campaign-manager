package net.thumbtack.campaign.manager.domain.repository;

import net.thumbtack.campaign.manager.domain.model.AdGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Collection;

/**
 * Repository interface to access AdGroup data.
 */
public interface AdGroupRepository extends PagingAndSortingRepository<AdGroup, Integer> {

    /**
     * Find AdGroup by it's name and Campaign id.
     * @param id Campaign's id
     * @param name Name of the Campaign
     * @return AdGroup with specified name
     */
    AdGroup findByCampaignIdAndName(Integer id, String name);

    /**
     * Find AdGroup by it's name and Campaign id.
     * @param id Campaign's id
     * @param name Name of the Campaign
     * @return AdGroup with specified name
     */
    Page<AdGroup> findByCampaignIdAndNameIgnoreCaseContaining(Integer id, String name, Pageable pageable);

    /**
     * Find all AdGroups by Campaign Id.
     * @param id Id of the Campaign
     * @return Collection of Campaign's AdGroups
     */
    Collection<AdGroup> findByCampaignId(Integer id);

    /**
     * Find page of AdGroups by Campaign Id.
     * @param id Id of the Campaign
     * @return Page of Campaign's AdGroups
     */
    Page<AdGroup> findByCampaignId(Integer id, Pageable pageable);
}
