package net.thumbtack.campaign.manager.domain.repository;

import net.thumbtack.campaign.manager.domain.model.CampaignStatus;
import net.thumbtack.campaign.manager.domain.model.CampaignStatusEnum;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository interface to access CampaignStatus data.
 */
public interface CampaignStatusRepository extends CrudRepository<CampaignStatus, Integer> {

    /**
     * Find CampaignStatus by it's Type.
     * @param status Type of the CampaignStatus
     * @return CampaignStatus with specified Type
     */
    CampaignStatus findByStatus(CampaignStatusEnum status);
}
