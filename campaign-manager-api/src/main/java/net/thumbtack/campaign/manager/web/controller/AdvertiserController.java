package net.thumbtack.campaign.manager.web.controller;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.thumbtack.campaign.manager.domain.model.Advertiser;
import net.thumbtack.campaign.manager.domain.repository.AdvertiserRepository;
import net.thumbtack.campaign.manager.web.exceptions.NotFoundException;
import net.thumbtack.campaign.manager.web.form.AdvertiserCreateForm;
import net.thumbtack.campaign.manager.web.form.AdvertiserUpdateForm;
import net.thumbtack.campaign.manager.web.utils.AdvertiserUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Advertiser controller.
 */
@RestController
@RequestMapping("/v1/advertisers")
@Transactional
public class AdvertiserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdvertiserController.class);

    @Autowired
    AdvertiserRepository advertiserRepository;

    /**
     * Get list of all Advertisers.
     * @return List of Advertisers
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @ApiOperation(value = "getAll", nickname = "getAllAdvertisers")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page."),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
                    value = "Sorting criteria in the format: property(,asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = Advertiser.class, responseContainer = "List"),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public Iterable<Advertiser> getAll(@ApiIgnore final Pageable pageable) {
        return advertiserRepository.findAll(pageable);
    }

    /**
     * Get Advertiser by id.
     * @return Advertiser with specified id
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @ApiOperation(value = "getById", nickname = "getAdvertiserById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "Advertiser's id", required = true, dataType = "int",
                    paramType = "path")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Advertiser.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET, path = "/{id}", produces = "application/json")
    public Advertiser getById(@PathVariable final Integer id) {
        Advertiser advertiser = advertiserRepository.findOne(id);
        if (advertiser == null) {
            throw new NotFoundException("Advertiser not found");
        }
        return advertiser;
    }

    /**
     * Find Advertisers by name.
     * @return Advertisers that fits search string
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @ApiOperation(value = "findById", nickname = "findAdvertiserById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "Advertiser's name", required = true,
                    dataType = "string", paramType = "query")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Advertiser.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET, path = "/search", produces = "application/json")
    public Iterable<Advertiser> getByName(@RequestParam final String name, @ApiIgnore final Pageable pageable) {
        return advertiserRepository.findByNameIgnoreCaseContaining(name, pageable);
//        if (advertiser == null) {
//            throw new NotFoundException("Advertiser not found");
//        }
//        return advertiser;
    }

    /**
     * Create new Advertiser.
     * @return created Advertiser
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "create", nickname = "createAdvertiser")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Advertiser.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.POST,
            produces = "application/json", consumes = "application/json")
    public Advertiser create(@RequestBody final AdvertiserCreateForm adForm) {
        Advertiser advertiser = AdvertiserUtils.buildAdvertiser(adForm);
        advertiserRepository.save(advertiser);
        LOGGER.info("Received: {}", adForm);
        return advertiser;
    }

    /**
     * Update Advertiser by id.
     * @return updated Advertiser
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "updateById", nickname = "updateAdvertiserById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "Advertiser's id", required = true, dataType = "int",
                    paramType = "path")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Advertiser.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.PUT, path = "/{id}",
            produces = "application/json", consumes = "application/json")
    public Advertiser updateById(@RequestBody final AdvertiserUpdateForm adForm, @PathVariable final Integer id) {
        Advertiser advertiser = advertiserRepository.findOne(id);
        if (advertiser == null) {
            throw new NotFoundException("Advertiser not found");
        }
        LOGGER.info("Update {} with {}", advertiser, adForm);
        advertiser = AdvertiserUtils.updateAdvertiser(advertiser, adForm);
        advertiserRepository.save(advertiser);
        return advertiser;
    }

    /**
     * Delete Advertiser by id.
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "deleteById", nickname = "deleteAdvertiserById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "Advertiser's id", required = true, dataType = "int",
                    paramType = "path")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.DELETE, path = "/{id}")
    public void deleteById(@PathVariable final Integer id) {
        advertiserRepository.delete(id);
        LOGGER.info("Delete Advertiser with id = {}", id);
    }
}
