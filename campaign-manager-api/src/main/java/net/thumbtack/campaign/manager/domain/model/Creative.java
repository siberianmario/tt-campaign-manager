package net.thumbtack.campaign.manager.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * A top level entity for Creative.
 */
@Entity
@Table(name = "creative")
public class Creative {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The id of the Creative", required = true)
    private Integer id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "ad_group_id")
    private AdGroup adGroup;

    @ApiModelProperty(notes = "The name of the Creative", required = true)
    private String name;

    @ApiModelProperty(notes = "The description of the Creative")
    private String description;

    @ApiModelProperty(notes = "The landing page url of the Creative", required = true)
    @Column(name = "landing_page_url")
    private String landingPageUrl;

    @ApiModelProperty(notes = "The height of the Creative", required = true)
    private Integer height;

    @ApiModelProperty(notes = "The width of the Creative", required = true)
    private Integer width;

    @OneToOne
    @JoinColumn(name = "type")
    @ApiModelProperty(notes = "The type of the Creative", required = true)
    private CreativeType type;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "asset")
    @ApiModelProperty(notes = "The CreativeAsset linked to the Creative", required = true)
    private CreativeAsset asset;

    @JsonIgnore
    @OneToMany(mappedBy = "creative", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Tracker> trackers = new ArrayList<>();


    public Creative() {
    }

    public Creative(AdGroup adGroup, String name, String description, String landingPageUrl,
                    Integer height, Integer width, CreativeType type, CreativeAsset asset) {
        this.adGroup = adGroup;
        this.name = name;
        this.description = description;
        this.landingPageUrl = landingPageUrl;
        this.height = height;
        this.width = width;
        this.type = type;
        this.asset = asset;
    }

    public Creative(String name, String description, String landingPageUrl, CreativeAsset asset) {
        this(null, name, description, landingPageUrl, null, null, null, asset);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AdGroup getAdGroup() {
        return adGroup;
    }

    public void setAdGroup(AdGroup adGroup) {
        this.adGroup = adGroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLandingPageUrl() {
        return landingPageUrl;
    }

    public void setLandingPageUrl(String landingPageUrl) {
        this.landingPageUrl = landingPageUrl;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public CreativeType getType() {
        return type;
    }

    public void setType(CreativeType type) {
        this.type = type;
    }

    public CreativeAsset getAsset() {
        return asset;
    }

    public void setAsset(CreativeAsset asset) {
        this.asset = asset;
    }

    public List<Tracker> getTrackers() {
        return trackers;
    }

    public void setTrackers(List<Tracker> trackers) {
        this.trackers = trackers;
    }

    @Override
    public String toString() {
        return "Creative{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", landingPageUrl='" + landingPageUrl + '\'' +
                ", height=" + height +
                ", width=" + width +
                ", type=" + type +
                ", asset=" + asset +
                '}';
    }
}
