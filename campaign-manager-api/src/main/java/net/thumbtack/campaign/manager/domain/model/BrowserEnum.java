package net.thumbtack.campaign.manager.domain.model;

/**
 * Browser types for AdGroup targeting.
 */
public enum BrowserEnum {
    CHROME, FIREFOX, OPERA, MICROSOFT_IE, SAFARI, KONQUEROR, OTHER
}
