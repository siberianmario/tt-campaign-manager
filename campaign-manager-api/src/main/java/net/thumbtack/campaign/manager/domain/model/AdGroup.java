package net.thumbtack.campaign.manager.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A top level entity for adGroup.
 */
@Entity
@Table(name = "ad_group")
public class AdGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The id of the AdGroup", required = true)
    private Integer id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "campaign_id")
    private Campaign campaign;

    @ApiModelProperty(notes = "The name of the AdGroup", required = true)
    private String name;

    @ApiModelProperty(notes = "The description of the AdGroup")
    private String description;

    @JsonIgnore
    @OneToMany(mappedBy = "adGroup", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Creative> creatives = new ArrayList<>();

    @ApiModelProperty(notes = "Target browsers of the AdGroup")
    @ElementCollection(targetClass = BrowserEnum.class, fetch = FetchType.EAGER)
    @JoinTable(name = "target_browser", joinColumns = @JoinColumn(name = "ad_group_id"))
    @Column(name = "browser", nullable = false)
    @Enumerated(EnumType.STRING)
    private Set<BrowserEnum> browsers = new HashSet<>();

    @ApiModelProperty(notes = "Target operating systems of the AdGroup")
    @ElementCollection(targetClass = OperatingSystemEnum.class, fetch = FetchType.EAGER)
    @JoinTable(name = "target_os", joinColumns = @JoinColumn(name = "ad_group_id"))
    @Column(name = "os", nullable = false)
    @Enumerated(EnumType.STRING)
    private Set<OperatingSystemEnum> operatingSystems = new HashSet<>();

    @ApiModelProperty(notes = "Target devices of the AdGroup")
    @ElementCollection(targetClass = DeviceEnum.class, fetch = FetchType.EAGER)
    @JoinTable(name = "target_device", joinColumns = @JoinColumn(name = "ad_group_id"))
    @Column(name = "device", nullable = false)
    @Enumerated(EnumType.STRING)
    private Set<DeviceEnum> devices = new HashSet<>();

    @ApiModelProperty(notes = "Schedule of the AdGroup")
    @OneToMany(mappedBy = "adGroup", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ScheduleDay> schedule = new ArrayList<>();

    public AdGroup() {
    }

    public AdGroup(Campaign campaign, String name, String description) {
        this.campaign = campaign;
        this.name = name;
        this.description = description;
    }

    public AdGroup(Campaign campaign, String name, String description, Set<BrowserEnum> browsers,
                   Set<OperatingSystemEnum> operatingSystems, Set<DeviceEnum> devices) {
        this.campaign = campaign;
        this.name = name;
        this.description = description;
        this.browsers = browsers;
        this.operatingSystems = operatingSystems;
        this.devices = devices;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Campaign getCampaign() {
        return campaign;
    }

    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Creative> getCreatives() {
        return creatives;
    }

    public void setCreatives(List<Creative> creatives) {
        this.creatives = creatives;
    }

    public Set<BrowserEnum> getBrowsers() {
        return browsers;
    }

    public void setBrowsers(Set<BrowserEnum> browsers) {
        this.browsers = browsers;
    }

    public Set<OperatingSystemEnum> getOperatingSystems() {
        return operatingSystems;
    }

    public void setOperatingSystems(Set<OperatingSystemEnum> operatingSystems) {
        this.operatingSystems = operatingSystems;
    }

    public Set<DeviceEnum> getDevices() {
        return devices;
    }

    public void setDevices(Set<DeviceEnum> devices) {
        this.devices = devices;
    }

    public List<ScheduleDay> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<ScheduleDay> schedule) {
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        return "AdGroup{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", browsers=" + browsers +
                ", operatingSystems=" + operatingSystems +
                ", devices=" + devices +
                ", schedule=" + schedule +
                '}';
    }
}
