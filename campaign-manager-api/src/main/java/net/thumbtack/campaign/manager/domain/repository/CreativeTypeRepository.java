package net.thumbtack.campaign.manager.domain.repository;

import net.thumbtack.campaign.manager.domain.model.CreativeType;
import net.thumbtack.campaign.manager.domain.model.CreativeTypeEnum;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository interface to access CreativeType data.
 */
public interface CreativeTypeRepository extends CrudRepository<CreativeType, Integer> {

    /**
     * Find CreativeType by it's Type.
     * @param type Type of the CreativeType
     * @return CreativeType with specified Type
     */
    CreativeType findByType(CreativeTypeEnum type);
}
