package net.thumbtack.campaign.manager.web.controller;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.thumbtack.campaign.manager.domain.model.Advertiser;
import net.thumbtack.campaign.manager.domain.model.Campaign;
import net.thumbtack.campaign.manager.domain.model.CampaignStatus;
import net.thumbtack.campaign.manager.domain.model.CampaignStatusEnum;
import net.thumbtack.campaign.manager.domain.repository.AdvertiserRepository;
import net.thumbtack.campaign.manager.domain.repository.CampaignRepository;
import net.thumbtack.campaign.manager.domain.repository.CampaignStatusRepository;
import net.thumbtack.campaign.manager.web.exceptions.NotFoundException;
import net.thumbtack.campaign.manager.web.form.CampaignCreateForm;
import net.thumbtack.campaign.manager.web.form.CampaignUpdateForm;
import net.thumbtack.campaign.manager.web.utils.CampaignUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Campaign Controller.
 */
@RestController
@RequestMapping("/v1")
@Transactional
public class CampaignController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CampaignController.class);

    @Autowired
    AdvertiserRepository advertiserRepository;

    @Autowired
    CampaignRepository campaignRepository;

    @Autowired
    CampaignStatusRepository campaignStatusRepository;

    /**
     * Get page of all Advertiser's Campaigns.
     * @return Page of Campaigns
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @ApiOperation(value = "getAll", nickname = "getAllCampaigns")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "advertiserId", value = "Advertiser's id", required = true,
                    dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page."),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
                    value = "Sorting criteria in the format: property(,asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Campaign.class, responseContainer = "List"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET, path = "/advertisers/{advertiserId}/campaigns",
            produces = "application/json")
    public Iterable<Campaign> getAll(@PathVariable final Integer advertiserId, @ApiIgnore final Pageable pageable) {
        Advertiser advertiser = advertiserRepository.findOne(advertiserId);
        if (advertiser == null) {
            throw new NotFoundException("Advertiser not found");
        }
        return campaignRepository.findByAdvertiserId(advertiserId, pageable);
    }

    /**
     * Get Campaign by id.
     * @return Campaign with specified id
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @ApiOperation(value = "getById", nickname = "getCampaignById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "campaignId", value = "Campaign's id", required = true, dataType = "int",
                    paramType = "path")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Campaign.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET, path = "/campaigns/{campaignId}",
            produces = "application/json")
    public Campaign getById(@PathVariable final Integer campaignId) {
        Campaign campaign = findCampaignById(campaignId);
        return campaign;
    }

    /**
     * Find Advertiser's Campaign by name.
     * @return Advertiser's Campaign with specified name
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @ApiOperation(value = "findById", nickname = "findCampaignById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "advertiserId", value = "Advertiser's id", required = true,
                    dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "name", value = "Campaign's name", required = true,
                    dataType = "string", paramType = "query")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Campaign.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET, path = "/advertisers/{advertiserId}/campaigns/search",
            produces = "application/json")
    public Iterable<Campaign> getByName(@PathVariable final Integer advertiserId, @RequestParam final String name,
                                        @ApiIgnore final Pageable pageable) {
        Advertiser advertiser = advertiserRepository.findOne(advertiserId);
        if (advertiser == null) {
            throw new NotFoundException("Advertiser not found");
        }
        return campaignRepository.findByAdvertiserIdAndNameIgnoreCaseContaining(advertiserId, name, pageable);
    }

    /**
     * Create new Campaign.
     * @return created Campaign
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "create", nickname = "createCampaign")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Campaign.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.POST, path = "/advertisers/{advertiserId}/campaigns",
            produces = "application/json", consumes = "application/json")
    public Campaign create(@PathVariable final Integer advertiserId, @RequestBody final CampaignCreateForm form) {
        Advertiser advertiser = advertiserRepository.findOne(advertiserId);
        if (advertiser == null) {
            throw new NotFoundException("Advertiser not found");
        }
        CampaignStatus draftStatus = campaignStatusRepository.findByStatus(CampaignStatusEnum.DRAFT);
        Campaign campaign = CampaignUtils.buildCampaign(form);
        campaign.setAdvertiser(advertiser);
        campaign.setStatus(draftStatus);
        campaignRepository.save(campaign);
        LOGGER.info("Created: {}", campaign);
        return campaign;
    }

    /**
     * Update Campaign by id.
     * @return updated Campaign
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "updateById", nickname = "updateCampaignById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "campaignId", value = "Campaign's id", required = true, dataType = "int",
                    paramType = "path")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Campaign.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.PUT, path = "/campaigns/{campaignId}",
            produces = "application/json", consumes = "application/json")
    public Campaign updateById(@RequestBody final CampaignUpdateForm form, @PathVariable final Integer campaignId) {
        Campaign campaign = findCampaignById(campaignId);
        LOGGER.info("Trying to update {} with {}", campaign, form);
        try {
            campaign = CampaignUtils.updateCampaign(campaign, form);
            CampaignStatus draftStatus = campaignStatusRepository.findByStatus(CampaignStatusEnum.DRAFT);
            campaign.setStatus(draftStatus);
            campaignRepository.save(campaign);
        } catch (RuntimeException ex) {
            LOGGER.info("Update failed: ", ex.getMessage());
            throw ex;
        }
        LOGGER.info("Updated successfully: {}", campaign);
        return campaign;
    }

    /**
     * Delete Campaign by id.
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "deleteById", nickname = "deleteCampaignById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "campaignId", value = "Campaign's id", required = true, dataType = "int",
                    paramType = "path")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.DELETE, path = "/campaigns/{campaignId}")
    public void deleteById(@PathVariable final Integer campaignId) {
        Campaign campaign = findCampaignById(campaignId);
        campaignRepository.delete(campaignId);
        LOGGER.info("Deleted: {}", campaign);
    }

    /**
     * Activate Campaign.
     * @return activated Campaign
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "activate", nickname = "activateCampaign")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Campaign.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.POST, path = "/campaigns/{campaignId}/activate",
            produces = "application/json", consumes = "application/json")
    public Campaign activate(@PathVariable final Integer campaignId) {
        Campaign campaign = findCampaignById(campaignId);
        CampaignStatusEnum newStatus = CampaignUtils.activate(campaign);
        campaign.setStatus(campaignStatusRepository.findByStatus(newStatus));
        campaignRepository.save(campaign);
        LOGGER.info("Activated: {}", campaign);
        return campaign;
    }

    /**
     * Pause Campaign.
     * @return paused Campaign
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "pause", nickname = "pauseCampaign")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Campaign.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.POST, path = "/campaigns/{campaignId}/pause",
            produces = "application/json", consumes = "application/json")
    public Campaign pause(@PathVariable final Integer campaignId) {
        Campaign campaign = findCampaignById(campaignId);
        CampaignStatusEnum newStatus = CampaignUtils.pause(campaign);
        campaign.setStatus(campaignStatusRepository.findByStatus(newStatus));
        campaignRepository.save(campaign);
        LOGGER.info("Paused: {}", campaign);
        return campaign;
    }

    /**
     * End Campaign.
     * @return ended Campaign
     */
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @ApiOperation(value = "ens", nickname = "endCampaign")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Campaign.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.POST, path = "/campaigns/{campaignId}/end",
            produces = "application/json", consumes = "application/json")
    public Campaign end(@PathVariable final Integer campaignId) {
        Campaign campaign = findCampaignById(campaignId);
        campaign.setStatus(campaignStatusRepository.findByStatus(CampaignStatusEnum.ENDED));
        campaignRepository.save(campaign);
        LOGGER.info("Ended: {}", campaign);
        return campaign;
    }

    private Campaign findCampaignById(Integer campaignId) {
        Campaign campaign = campaignRepository.findOne(campaignId);
        if (campaign == null) {
            throw new NotFoundException("Campaign not found");
        }
        return campaign;
    }
}
