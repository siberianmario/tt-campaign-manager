package net.thumbtack.campaign.manager.web.utils;

import edu.emory.mathcs.backport.java.util.Arrays;
import net.thumbtack.campaign.manager.domain.model.ScheduleDay;
import net.thumbtack.campaign.manager.web.exceptions.ValidationException;
import net.thumbtack.campaign.manager.web.form.AdGroupCreateForm;
import net.thumbtack.campaign.manager.web.form.AdGroupUpdateForm;
import net.thumbtack.campaign.manager.web.form.AdvertiserCreateForm;
import net.thumbtack.campaign.manager.web.form.AdvertiserUpdateForm;
import net.thumbtack.campaign.manager.web.form.CampaignCreateForm;
import net.thumbtack.campaign.manager.web.form.CampaignUpdateForm;
import net.thumbtack.campaign.manager.web.form.CreativeCreateForm;
import net.thumbtack.campaign.manager.web.form.CreativeUpdateForm;
import net.thumbtack.campaign.manager.web.form.TrackerCreateForm;
import net.thumbtack.campaign.manager.web.form.TrackerUpdateForm;
import net.thumbtack.campaign.manager.web.form.UserCreateForm;
import net.thumbtack.campaign.manager.web.form.UserUpdateForm;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Validation Utils.
 */
public class ValidationUtils {

    private static final String[] ROLES_ARRAY = new String[] {"ROLE_USER", "ROLE_MANAGER", "ROLE_ADMIN"};
    public static final Set<String> ACCEPTABLE_ROLES = new HashSet<String>(Arrays.asList(ROLES_ARRAY));

    /**
     * Validate AdvertiserCreateForm.
     * @param adForm AdvertiserCreateForm to check
     */
    public static void validateAdvertiserCreateForm(AdvertiserCreateForm adForm) {
        if (StringUtils.isEmpty(adForm.getName())) {
            throw new ValidationException("Advertiser name can't be null or empty");
        }
    }

    /**
     * Validate AdvertiserUpdateForm.
     * @param adForm AdvertiserUpdateForm to check
     */
    public static void validateAdvertiserUpdateForm(AdvertiserUpdateForm adForm) {
        if (adForm.getName() != null) {
            if (StringUtils.isEmpty(adForm.getName().orElse(null))) {
                throw new ValidationException("Advertiser name can't be null or empty");
            }
        }
    }

    /**
     * Validate UserCreateForm.
     * @param userForm UserCreateForm to check
     */
    public static void validateUserCreateForm(UserCreateForm userForm) {
        if (StringUtils.isEmpty(userForm.getUsername())) {
            throw new ValidationException("Username can't be null or empty");
        }
        if (StringUtils.isEmpty(userForm.getPassword())) {
            throw new ValidationException("Password can't be null or empty");
        }
        if (StringUtils.isEmpty(userForm.getFirstName())) {
            throw new ValidationException("First name can't be null or empty");
        }
        if (StringUtils.isEmpty(userForm.getLastName())) {
            throw new ValidationException("Last name can't be null or empty");
        }
        if (userForm.getRoles() != null) {
            validateUserRoles(userForm.getRoles());
        }
    }

    /**
     * Validate UserRoles.
     * @param roles UserRoles to check
     */
    public static void validateUserRoles(Set<String> roles) {
        if (!ACCEPTABLE_ROLES.containsAll(roles)) {
            throw new ValidationException("One or more roles are invalid");
        }
    }

    /**
     * Validate UserUpdateForm.
     * @param userForm UserUpdateForm to check
     */
    public static void validateUserUpdateForm(UserUpdateForm userForm) {
        if (userForm.getPassword() != null) {
            if (StringUtils.isEmpty(userForm.getPassword().orElse(null))) {
                throw new ValidationException("User password can't be null or empty");
            }
        }
        if (userForm.getFirstName() != null) {
            if (StringUtils.isEmpty(userForm.getFirstName().orElse(null))) {
                throw new ValidationException("User first name can't be null or empty");
            }
        }
        if (userForm.getLastName() != null) {
            if (StringUtils.isEmpty(userForm.getLastName().orElse(null))) {
                throw new ValidationException("User last name can't be null or empty");
            }
        }
    }

    /**
     * Validate CampaignCreateForm.
     * @param form CampaignCreateForm to check
     */
    public static void validateCampaignCreateForm(CampaignCreateForm form) {
        if (StringUtils.isEmpty(form.getName())) {
            throw new ValidationException("Campaign name can't be null or empty");
        }
        if (form.getStartDate() == null) {
            throw new ValidationException("Campaign start date can't be null");
        }
        if (form.getEndDate() == null) {
            throw new ValidationException("Campaign end date can't be null");
        }
        if (form.getEndDate().before(form.getStartDate())) {
            throw new ValidationException("Campaign end date can't be earlier than start date");
        }
        if (form.getLifetimeImpCap() == null || form.getLifetimeImpCap() < 1) {
            throw new ValidationException("Campaign lifetime impression capacity can't be null, negative or zero");
        }
        if (form.getLifetimeBudget() == null || form.getLifetimeBudget().compareTo(BigDecimal.ZERO) < 1) {
            throw new ValidationException("Campaign lifetime budget can't be null, negative or zero");
        }
    }

    /**
     * Validate CampaignUpdateForm.
     * @param form CampaignUpdateForm to check
     */
    public static void validateCampaignUpdateForm(CampaignUpdateForm form) {
        if (form.getName() != null) {
            if (StringUtils.isEmpty(form.getName().orElse(null))) {
                throw new ValidationException("Campaign name can't be null or empty");
            }
        }
        if (form.getStartDate() != null) {
            if (!form.getStartDate().isPresent()) {
                throw new ValidationException("Campaign start date can't be null");
            }
        }
        if (form.getEndDate() != null) {
            if (!form.getEndDate().isPresent()) {
                throw new ValidationException("Campaign end date can't be null");
            }
        }
        if (form.getLifetimeImpCap() != null) {
            if (!form.getLifetimeImpCap().isPresent() || form.getLifetimeImpCap().get() < 1) {
                throw new ValidationException("Campaign lifetime impression capacity can't be null, negative or zero");
            }
        }
        if (form.getLifetimeBudget() != null) {
            if (!form.getLifetimeBudget().isPresent()
                    || form.getLifetimeBudget().get().compareTo(BigDecimal.ZERO) < 1) {
                throw new ValidationException("Campaign lifetime budget can't be null, negative or zero");
            }
        }
    }

    /**
     * Validate AdGroupCreateForm.
     * @param form AdGroupCreateForm to check
     */
    public static void validateAdGroupCreateForm(AdGroupCreateForm form) {
        if (StringUtils.isEmpty(form.getName())) {
            throw new ValidationException("AdGroup name can't be null or empty");
        }
    }

    /**
     * Validate AdGroupUpdateForm.
     * @param form AdGroupUpdateForm to check
     */
    public static void validateAdGroupUpdateForm(AdGroupUpdateForm form) {
        if (form.getName() != null) {
            if (StringUtils.isEmpty(form.getName().orElse(null))) {
                throw new ValidationException("AdGroup name can't be null or empty");
            }
        }
        if (form.getSchedule() != null) {
            validateSchedule(form.getSchedule().orElse(null));
        }
    }

    private static void validateSchedule(List<ScheduleDay> schedule) {
        if (schedule != null) {
            schedule.forEach(scheduleDay ->
                    scheduleDay.getHours().forEach(scheduleHours -> {
                        int start = scheduleHours.getStartHour();
                        int end = scheduleHours.getEndHour();
                        if (start < 0 || start > 23 || end < 0 || end > 23) {
                            throw new ValidationException("Schedule hours must be in range [0, 23]");
                        }
                        if (start > end) {
                            throw new ValidationException("Schedule start hour should be less or equal to end hour");
                        }
                    })
            );
        }
    }

    /**
     * Validate CreativeCreateForm.
     * @param form CreativeCreateForm to check
     */
    public static void validateCreativeCreateForm(CreativeCreateForm form) {
        if (StringUtils.isEmpty(form.getName())) {
            throw new ValidationException("Creative name can't be null or empty");
        }
        if (StringUtils.isEmpty(form.getLandingPageUrl())) {
            throw new ValidationException("Creative's landing page url can't be null or empty");
        }
        if (form.getType() == null) {
            throw new ValidationException("Creative type can't be null");
        }
    }

    /**
     * Validate CreativeUpdateForm.
     * @param form CreativeUpdateForm to check
     */
    public static void validateCreativeUpdateForm(CreativeUpdateForm form) {
        if (form.getName() != null) {
            if (StringUtils.isEmpty(form.getName().orElse(null))) {
                throw new ValidationException("Creative name can't be null or empty");
            }
        }
        if (form.getLandingPageUrl() != null) {
            if (StringUtils.isEmpty(form.getLandingPageUrl().orElse(null))) {
                throw new ValidationException("Creative's landing page URL can't be null or empty");
            }
        }
    }

    /**
     * Validate TrackerCreateForm.
     * @param form TrackerCreateForm to check
     */
    public static void validateTrackerCreateForm(TrackerCreateForm form) {
        if (StringUtils.isEmpty(form.getBody())) {
            throw new ValidationException("Tracker body can't be null or empty");
        }
        if (form.getType() == null) {
            throw new ValidationException("Tracker type can't be null");
        }
    }

    /**
     * Validate TrackerUpdateForm.
     * @param form TrackerUpdateForm to check
     */
    public static void validateTrackerUpdateForm(TrackerUpdateForm form) {
        if (form.getBody() != null) {
            if (StringUtils.isEmpty(form.getBody().orElse(null))) {
                throw new ValidationException("Tracker body can't be null or empty");
            }
        }
    }
}
