package net.thumbtack.campaign.manager.domain.model;

/**
 * Device types for AdGroup targeting.
 */
public enum DeviceEnum {
    DESKTOP, MOBILE, TABLET
}
