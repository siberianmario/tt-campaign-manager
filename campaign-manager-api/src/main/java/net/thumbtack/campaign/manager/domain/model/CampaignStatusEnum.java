package net.thumbtack.campaign.manager.domain.model;

/**
 * Campaign Status Enum.
 */
public enum CampaignStatusEnum { DRAFT, ACTIVE, PENDING, PAUSED, ENDED
}
