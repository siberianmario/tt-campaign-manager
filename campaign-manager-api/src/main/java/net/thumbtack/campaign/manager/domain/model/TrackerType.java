package net.thumbtack.campaign.manager.domain.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * A top level entity for TrackerType.
 */
@Entity
@Table(name = "tracker_type")
public class TrackerType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private TrackerTypeEnum type;

    protected TrackerType() {
    }

    public TrackerType(TrackerTypeEnum type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TrackerTypeEnum getType() {
        return type;
    }

    public void setType(TrackerTypeEnum type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "TrackerType{" +
                "id=" + id +
                ", type=" + type +
                '}';
    }
}
