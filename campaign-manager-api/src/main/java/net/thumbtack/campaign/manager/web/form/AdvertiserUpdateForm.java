package net.thumbtack.campaign.manager.web.form;

import io.swagger.annotations.ApiModelProperty;

import java.util.Optional;

/**
 * Form for updating Advertiser.
 */
public class AdvertiserUpdateForm {

    @ApiModelProperty(notes = "The name of the Advertiser")
    private Optional<String> name;

    @ApiModelProperty(notes = "The description of the Advertiser")
    private Optional<String> description;

    @ApiModelProperty(notes = "The site of the Advertiser")
    private Optional<String> site;

    protected AdvertiserUpdateForm() {
    }

    public AdvertiserUpdateForm(Optional<String> name, Optional<String> description, Optional<String> site) {
        this.name = name;
        this.description = description;
        this.site = site;
    }

    public Optional<String> getName() {
        return name;
    }

    public Optional<String> getDescription() {
        return description;
    }

    public Optional<String> getSite() {
        return site;
    }

    @Override
    public String toString() {
        return "AdvertiserUpdateForm{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", site='" + site + '\'' +
                '}';
    }
}
