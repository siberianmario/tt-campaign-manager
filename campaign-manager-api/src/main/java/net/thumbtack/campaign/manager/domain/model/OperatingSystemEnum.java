package net.thumbtack.campaign.manager.domain.model;

/**
 * OS types for AdGroup targeting.
 */
public enum OperatingSystemEnum {
    WINDOWS, LINUX, OSX, IOS, ANDROID, OTHER
}
