package net.thumbtack.campaign.manager.security;

import net.thumbtack.campaign.manager.domain.model.User;
import net.thumbtack.campaign.manager.domain.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Custom implementation of UserDetailsService interface.
 */
@Service
@Transactional
public class CustomUserDetailsService implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomUserDetailsService.class);

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            LOGGER.info("No user present with username: " + username);
            throw new UsernameNotFoundException("No user present with username: " + username);
        } else {
            CustomUserDetails userDetails = new CustomUserDetails(user);
            LOGGER.info("Authenticating with: " + userDetails.toString());
            return userDetails;
        }
    }
}
