package net.thumbtack.campaign.manager.web.utils;

import net.thumbtack.campaign.manager.domain.model.Advertiser;
import net.thumbtack.campaign.manager.web.form.AdvertiserCreateForm;
import net.thumbtack.campaign.manager.web.form.AdvertiserUpdateForm;

/**
 * Advertiser Utils.
 */
public class AdvertiserUtils {
    /**
     * Build Advertiser object with CreateForm parameters.
     * @param adForm CreateForm with parameters
     * @return new Advertiser
     */
    public static Advertiser buildAdvertiser(AdvertiserCreateForm adForm) {
        ValidationUtils.validateAdvertiserCreateForm(adForm);
        return new Advertiser(adForm.getName(), adForm.getDescription(), adForm.getSite());
    }

    /**
     * Update Advertiser with non-null UpdateForm parameters.
     * @param advertiser Advertiser to update
     * @param adForm UpdateForm with parameters
     * @return updated Advertiser
     */
    public static Advertiser updateAdvertiser(Advertiser advertiser, AdvertiserUpdateForm adForm) {
        ValidationUtils.validateAdvertiserUpdateForm(adForm);
        if (adForm.getName() != null) {
            advertiser.setName(adForm.getName().get());
        }
        if (adForm.getDescription() != null) {
            advertiser.setDescription(adForm.getDescription().orElse(null));
        }
        if (adForm.getSite() != null) {
            advertiser.setSite(adForm.getSite().orElse(null));
        }
        return advertiser;
    }
}
