package net.thumbtack.campaign.manager.web.form;

import io.swagger.annotations.ApiModelProperty;
import net.thumbtack.campaign.manager.domain.model.CreativeTypeEnum;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Optional;

/**
 * Form for updating Creative.
 */
public class CreativeUpdateForm {

    @ApiModelProperty(notes = "The name of the Creative", required = true, example = "name")
    private Optional<String> name;

    @ApiModelProperty(notes = "The description of the Creative", example = "description")
    private Optional<String> description;

    @ApiModelProperty(notes = "The landing page url of the Creative", required = true, example = "url")
    private Optional<String> landingPageUrl;

    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "The type of the Creative", required = true)
    private CreativeTypeEnum type;

    public CreativeUpdateForm() {
    }

    public CreativeUpdateForm(Optional<String> name, Optional<String> description,
                              Optional<String> landingPageUrl, CreativeTypeEnum type) {
        this.name = name;
        this.description = description;
        this.landingPageUrl = landingPageUrl;
        this.type = type;
    }

    public Optional<String> getName() {
        return name;
    }

    public Optional<String> getDescription() {
        return description;
    }

    public Optional<String> getLandingPageUrl() {
        return landingPageUrl;
    }

    public CreativeTypeEnum getType() {
        return type;
    }

    @Override
    public String toString() {
        return "CreativeUpdateForm{" +
                "name=" + name +
                ", description=" + description +
                ", landingPageUrl=" + landingPageUrl +
                ", type=" + type +
                '}';
    }
}
