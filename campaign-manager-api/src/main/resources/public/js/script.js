$(function () {

	// Globals variables

	var advertisersTemplate = Handlebars.compile($("#advertisers-table-template").html());
    var advertisersUpdateTemplate = Handlebars.compile($("#advertisers-update-template").html());
    var deleteModalTemplate = Handlebars.compile($("#delete-modal-template").html());

    var campaignsTemplate = Handlebars.compile($("#campaigns-table-template").html());
    var campaignsEditTemplate = Handlebars.compile($('#campaigns-edit-template').html());

    var adGroupsTemplate = Handlebars.compile($("#adgroups-table-template").html());
    var adGroupsEditTemplate = Handlebars.compile($('#adgroups-edit-template').html());

    var singleAdGroupTemplate = Handlebars.compile($("#single-adgroup-template").html());
    var scheduleListTemplate = Handlebars.compile($("#targeting-schedule-list-template").html());

    var creativesTemplate = Handlebars.compile($("#creatives-table-template").html());
    var creativesEditTemplate = Handlebars.compile($('#creatives-edit-template').html());
    var creativesEditContentTemplate = Handlebars.compile($('#creatives-edit-content-template').html());

    var singleCreativeTemplate = Handlebars.compile($('#single-creative-template').html());

    var pagination;
    var content;
    var navigation = [
        {type: "advertiser", subPath: "/campaigns"},
        {type: "campaign", subPath: "/adgroups"},
        {type: "adgroup", subPath: ""},
        {type: "creative", subPath: ""}
    ];

    var dateFormat = "YYYY-MM-DD HH:mm Z";

    bindGlobalUIEvents();
    resetPagination();
    resetNavigation();

    var advertisersController = function () {
        $.getJSON( "v1/advertisers",
            {page: pagination.number, size: pagination.size, sort: 'id'}, function( data ) {
                content = data.content;
                renderAdvertisersPage(data);
            });
    };

    var searchAdvertisersController = function () {
        var params = getUrlParams();
        $.getJSON( "v1/advertisers/search",{name: params.name, page: pagination.number, size: pagination.size})
            .done(function( data ) {
                content = data.content;
                renderAdvertisersPage(data);
            }).fail(function () {
                content = null;
                renderAdvertisersPage({content: content});
            });
    };

    var campaignsController = function (advertiserId) {
        $.getJSON( "v1/advertisers/" + advertiserId + "/campaigns",
            {page: pagination.number, size: pagination.size, sort: 'id'}, function( data ) {
                content = data.content;
                renderCampaignsPage(data);
            });
    };

    var searchCampaignsController = function (advertiserId) {
        var params = getUrlParams();
        $.getJSON( "v1/advertisers/" + advertiserId + "/campaigns/search",
            {name: params.name, page: pagination.number, size: pagination.size})
            .done(function( data ) {
                content = data.content;
                renderCampaignsPage(data);
            }).fail(function () {
                content = null;
                renderCampaignsPage({content: content});
            });
    };

    var adGroupsController = function (advertiserId, campaignId) {
        $.getJSON( "v1/campaigns/" + campaignId + "/adgroups",
            {page: pagination.number, size: pagination.size, sort: 'id'}, function( data ) {
                content = data.content;
                renderAdGroupsPage(data);
            });
    };

    var searchAdGroupsController = function (advertiserId, campaignId) {
        var params = getUrlParams();
        $.getJSON( "v1/campaigns/" + campaignId + "/adgroups/search",
            {name: params.name, page: pagination.number, size: pagination.size})
            .done(function( data ) {
                content = data.content;
                renderAdGroupsPage(data);
            }).fail(function () {
            content = null;
            renderAdGroupsPage({content: content});
        });
    };

    var singleAdGroupController = function (advertiserId, campaignId, adGroupId) {
        $.getJSON( "v1/adgroups/" + adGroupId + "/creatives",
            {page: pagination.number, size: pagination.size, sort: 'id'}, function( data ) {
                content = data.content;
                renderSingleAdGroupPage(data);
            });
    };

    var searchCreativesController = function (advertiserId, campaignId, adGroupId) {
        var params = getUrlParams();
        $.getJSON( "v1/adgroups/" + adGroupId + "/creatives/search",
            {name: params.name, page: pagination.number, size: pagination.size})
            .done(function( data ) {
                content = data.content;
                renderSingleAdGroupPage(data);
            }).fail(function () {
                content = null;
                renderSingleAdGroupPage({content: content});
        });
    };

    var singleCreativeController = function (advertiserId, campaignId, adGroupId, creativeId) {
        $.getJSON( "v1/creatives/" + creativeId + "/trackers", function( data ) {
                content = data;
                renderSingleCreativePage(data);
            });
    };

    var routes = {
        '/': function() {
            router.setRoute('/advertisers');
        },
        '/advertisers': {
            '/search': searchAdvertisersController,
            '/:id/campaigns': {
                '/search': searchCampaignsController,
                '/:id/adgroups': {
                    '/search': searchAdGroupsController,
                    '/:id': {
                        on: singleAdGroupController,
                        '/search': searchCreativesController,
                        '/creatives/:id': singleCreativeController
                    },
                    on: adGroupsController
                },
                on: campaignsController
            },
            on: advertisersController
        }
    };

    var router = Router(routes).configure({
        notfound: renderErrorPage,
        on: function () {
            updateNavigation.apply(this, arguments);
            $('.modal.in').modal('hide');
        }
    });

    router.init('/');

    // Read a page's GET URL variables and return them as an associative array.
    function getUrlParams() {

        var url = decodeURI(window.location.href);
        var vars = {}, hash;
        if (url.indexOf('?') < 0) {
            return vars;
        }
        var hashes = url.slice(url.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    function renderAdvertisersPage(data) {

        updatePaginationWithData(data);

        // update modals content

        var createModal = $('#createModal');
        var updateModal = $('#updateModal');

        var advertisers = data.content;
        var page = $('.main-page');
        var entityField = page.find('.entity-content');
        var table = page.find('.main-table');

        entityField.html("");

        page.find('.table-title').text("Advertisers");
        table.html(advertisersTemplate(advertisers));

        table.off('click').on('click', 'a', function (e) {
            e.preventDefault();
            var id = $(e.target).closest('tr').data('index');
            var advertiser = getContentObjectById(id);
            pushInNavigation('advertiser', advertiser);
            router.setRoute('/advertisers/' + id + '/campaigns');
        });

        page.find('.btn-create').on('click', function (e) {
            e.preventDefault();
            var createModal = $('#createModal');

            prepareModal(createModal, $('#advertisers-create-template').html(), function (e) {
                e.preventDefault();
                var form = createModal.find('form');
                if (validateForm(form)) {
                    $.ajax({
                        method: "POST",
                        url: "v1/advertisers",
                        contentType: 'application/json',
                        data: JSON.stringify(getObjectFromForm(form))
                    })
                        .done(function() {
                            createModal.modal('hide');
                            $(window).trigger('hashchange');
                        })
                        .fail(function (data) {
                            var alert = createModal.find('.error-alert');
                            alert.html("Error: " + data.responseJSON.message);
                            alert.show();
                        });
                }
            });

            createModal.modal('show');
        });

        table.on('click', '.btn-edit', function (e) {
            e.preventDefault();
            var updateModal = $('#updateModal');
            var id = $(this).closest('tr').data('index');
            var advertiser = getContentObjectById(id);

            prepareModal(updateModal, advertisersUpdateTemplate(advertiser), function(e) {
                e.preventDefault();
                var form = updateModal.find('form');
                if (validateForm(form)) {
                    $.ajax({
                        method: "PUT",
                        url: "v1/advertisers/" + id,
                        contentType: 'application/json',
                        data: JSON.stringify(getObjectFromForm(form))
                    })
                        .done(function() {
                            updateModal.modal('hide');
                            $(window).trigger('hashchange');
                        })
                        .fail(function (data) {
                            var alert = updateModal.find('.error-alert');
                            alert.html("Error: " + data.responseJSON.message);
                            alert.show();
                        });
                }
            });

            updateModal.modal('show');
        });

        table.on('click', '.btn-delete', function (e) {
            e.preventDefault();
            var deleteModal = $('#deleteModal');
            var id = $(this).closest('tr').data('index');
            var advertiser = getContentObjectById(id);
            advertiser.type = "Advertiser";

            deleteModal.html(deleteModalTemplate(advertiser));

            deleteModal.find('#btn-delete-modal').on('click', function () {
                $.ajax({
                    method: "DELETE",
                    url: "v1/advertisers/" + advertiser.id
                })
                    .done(function() {
                        deleteModal.modal('hide');
                        $(window).trigger('hashchange');
                    })
                    .fail(function (data) {
                        var alert = deleteModal.find('.error-alert');
                        alert.html("Error: " + data.responseJSON.message);
                        alert.show();
                    });
            });

            deleteModal.modal('show');
        });

        swapPagesIfNeeded(page);
    }

    function renderCampaignsPage(data) {

        updatePaginationWithData(data);

        // update modals content

        var createModal = $('#createModal');
        var updateModal = $('#updateModal');

        var campaigns = data.content;
        formatCampaignDates(campaigns);

        var page = $('.main-page');
        var entityField = page.find('.entity-content');
        var table = page.find('.main-table');

        entityField.html("");

        page.find('.table-title').text("Campaigns");
        table.html(campaignsTemplate(campaigns));

        table.off('click').on('click', '.entity-name-link', function (e) {
            e.preventDefault();
            var campaignId = $(e.target).closest('tr').data('index');
            var campaign = getContentObjectById(campaignId);
            pushInNavigation('campaign', campaign);
            var advertiser = getEntityFromNavigation('advertiser');
            router.setRoute('/advertisers/' + advertiser.id + '/campaigns/' + campaign.id + '/adgroups');
        });

        page.find('.btn-create').on('click', function (e) {
            e.preventDefault();
            var createModal = $('#createModal');

            prepareModal(createModal, campaignsEditTemplate(), function (e) {
                e.preventDefault();
                var advertiser = getEntityFromNavigation("advertiser");
                var form = createModal.find('form');
                if (validateForm(form)) {
                    var formContent = getObjectFromForm(form);
                    formContent.startDate = dateToTimestamp(formContent.startDate);
                    formContent.endDate = dateToTimestamp(formContent.endDate);
                    $.ajax({
                        method: "POST",
                        url: "v1/advertisers/" + advertiser.id + "/campaigns",
                        contentType: 'application/json',
                        data: JSON.stringify(formContent)
                    })
                        .done(function() {
                            createModal.modal('hide');
                            $(window).trigger('hashchange');
                        })
                        .fail(function (data) {
                            var alert = createModal.find('.error-alert');
                            alert.html("Error: " + data.responseJSON.message);
                            alert.show();
                        });
                }
            });
            createModal.on('shown.bs.modal', function () {
                $(this).find('.form-control:first').focus()
            });

            var datepickers = createModal.find('.date');
            datepickers.datetimepicker({format: dateFormat});

            createModal.modal('show');
        });

        table.on('click', '.btn-edit', function (e) {
            e.preventDefault();
            var updateModal = $('#updateModal');
            var id = $(this).closest('tr').data('index');
            var campaign = getContentObjectById(id);

            prepareModal(updateModal, campaignsEditTemplate(campaign), function(e) {
                e.preventDefault();
                var form = updateModal.find('form');
                if (validateForm(form)) {
                    var formContent = getObjectFromForm(form);
                    formContent.startDate = dateToTimestamp(formContent.startDate);
                    formContent.endDate = dateToTimestamp(formContent.endDate);
                    $.ajax({
                        method: "PUT",
                        url: "v1/campaigns/" + id,
                        contentType: 'application/json',
                        data: JSON.stringify(formContent)
                    })
                        .done(function() {
                            updateModal.modal('hide');
                            $(window).trigger('hashchange');
                        })
                        .fail(function (data) {
                            var alert = updateModal.find('.error-alert');
                            alert.html("Error: " + data.responseJSON.message);
                            alert.show();
                        });
                }
            });
            updateModal.on('shown.bs.modal', function () {
                $(this).find('.form-control:first').focus()
            });

            var datepickers = updateModal.find('.date');
            datepickers.datetimepicker({format: dateFormat});

            updateModal.modal('show');
        });

        table.on('click', '.btn-delete', function (e) {
            e.preventDefault();
            var deleteModal = $('#deleteModal');
            var id = $(this).closest('tr').data('index');
            var campaign = getContentObjectById(id);
            campaign.type = "Campaign";

            deleteModal.html(deleteModalTemplate(campaign));

            deleteModal.find('#btn-delete-modal').on('click', function () {
                $.ajax({
                    method: "DELETE",
                    url: "v1/campaigns/" + campaign.id
                })
                    .done(function() {
                        deleteModal.modal('hide');
                        $(window).trigger('hashchange');
                    })
                    .fail(function (data) {
                        var alert = deleteModal.find('.error-alert');
                        alert.html("Error: " + data.responseJSON.message);
                        alert.show();
                    });
            });

            deleteModal.modal('show');
        });

        table.on('click', '.campaign-activate', function (e) {
            e.preventDefault();
            var id = $(this).closest('tr').data('index');
            $.ajax({
                method: "POST",
                url: "v1/campaigns/" + id + "/activate",
                contentType: 'application/json'
            })
                .done(function() {
                    $(window).trigger('hashchange');
                });
        });

        table.on('click', '.campaign-pause', function (e) {
            e.preventDefault();
            var id = $(this).closest('tr').data('index');
            $.ajax({
                method: "POST",
                url: "v1/campaigns/" + id + "/pause",
                contentType: 'application/json'
            })
                .done(function() {
                    $(window).trigger('hashchange');
                });
        });

        table.on('click', '.campaign-end', function (e) {
            e.preventDefault();
            var id = $(this).closest('tr').data('index');
            $.ajax({
                method: "POST",
                url: "v1/campaigns/" + id + "/end",
                contentType: 'application/json'
            })
                .done(function() {
                    $(window).trigger('hashchange');
                });
        });

        updateStatusButtons(table);

        swapPagesIfNeeded(page);
    }

    function renderAdGroupsPage(data) {

        updatePaginationWithData(data);

        // update modals content

        var createModal = $('#createModal');
        var updateModal = $('#updateModal');

        var content = data.content;
        var page = $('.main-page');
        var entityField = page.find('.entity-content');
        var table = page.find('.main-table');

        entityField.html("");

        page.find('.table-title').text("AdGroups");
        table.html(adGroupsTemplate(content));

        table.off('click').on('click', 'a', function (e) {
            e.preventDefault();
            var adGroupId = $(e.target).closest('tr').data('index');
            var adGroup = getContentObjectById(adGroupId);
            pushInNavigation('adgroup', adGroup);
            var advertiser = getEntityFromNavigation('advertiser');
            var campaign = getEntityFromNavigation('campaign');
            router.setRoute('/advertisers/' + advertiser.id + '/campaigns/' + campaign.id
                + '/adgroups/' + adGroup.id);
        });

        page.find('.btn-create').on('click', function (e) {
            e.preventDefault();
            var createModal = $('#createModal');

            prepareModal(createModal, adGroupsEditTemplate(), function (e) {
                e.preventDefault();
                var campaign = getEntityFromNavigation("campaign");
                var form = createModal.find('form');
                if (validateForm(form)) {
                    $.ajax({
                        method: "POST",
                        url: "v1/campaigns/" + campaign.id + "/adgroups",
                        contentType: 'application/json',
                        data: JSON.stringify(getObjectFromForm(form))
                    })
                        .done(function() {
                            createModal.modal('hide');
                            $(window).trigger('hashchange');
                        })
                        .fail(function (data) {
                            var alert = createModal.find('.error-alert');
                            alert.html("Error: " + data.responseJSON.message);
                            alert.show();
                        });
                }
            });

            createModal.modal('show');
        });

        table.on('click', '.btn-edit', function (e) {
            e.preventDefault();
            var updateModal = $('#updateModal');
            var id = $(this).closest('tr').data('index');
            var adGroup = getContentObjectById(id);

            prepareModal(updateModal, adGroupsEditTemplate(adGroup), function(e) {
                e.preventDefault();
                var form = updateModal.find('form');
                if (validateForm(form)) {
                    $.ajax({
                        method: "PUT",
                        url: "v1/adgroups/" + id,
                        contentType: 'application/json',
                        data: JSON.stringify(getObjectFromForm(form))
                    })
                        .done(function() {
                            updateModal.modal('hide');
                            $(window).trigger('hashchange');
                        })
                        .fail(function (data) {
                            var alert = updateModal.find('.error-alert');
                            alert.html("Error: " + data.responseJSON.message);
                            alert.show();
                        });
                }
            });

            updateModal.modal('show');
        });

        table.on('click', '.btn-delete', function (e) {
            e.preventDefault();
            var deleteModal = $('#deleteModal');
            var id = $(this).closest('tr').data('index');
            var adGroup = getContentObjectById(id);
            adGroup.type = "AdGroup";

            deleteModal.html(deleteModalTemplate(adGroup));

            deleteModal.find('#btn-delete-modal').on('click', function () {
                $.ajax({
                    method: "DELETE",
                    url: "v1/adgroups/" + id
                })
                    .done(function() {
                        deleteModal.modal('hide');
                        $(window).trigger('hashchange');
                    })
                    .fail(function (data) {
                        var alert = deleteModal.find('.error-alert');
                        alert.html("Error: " + data.responseJSON.message);
                        alert.show();
                    });
            });

            deleteModal.modal('show');
        });

        swapPagesIfNeeded(page);
    }

    function renderSingleAdGroupPage(data) {

        updatePaginationWithData(data);

        var createModal = $('#createModal');
        var updateModal = $('#updateModal');

        var adGroup = getEntityFromNavigation('adgroup');
        var content = data.content;

        var page = $('.main-page');
        var entityField = page.find('.entity-content');
        var table = page.find('.main-table');

        entityField.html(singleAdGroupTemplate(adGroup));

        entityField.find('.targeting-browser .btn').on('click', function (e) {
            e.preventDefault();
            var updateModal = $('#updateModal');
            var adGroup = getEntityFromNavigation('adgroup');

            updateModal.html($("#targeting-browsers-template").html());
            fillTableWithCheckboxes(updateModal.find('table'), adGroup.browsers);

            updateModal.find('.submit-btn').on('click', function (e) {
                e.preventDefault();
                var table = $('#updateModal').find('table');
                var adGroup = getEntityFromNavigation('adgroup');
                var data = {};
                data.browsers = getCheckboxesFromTable(table);
                putUpdateAdGroup(adGroup, data);
            });

            updateModal.modal('show');
        });

        entityField.find('.targeting-device .btn').on('click', function (e) {
            e.preventDefault();
            var updateModal = $('#updateModal');
            var adGroup = getEntityFromNavigation('adgroup');

            updateModal.html($("#targeting-devices-template").html());
            fillTableWithCheckboxes(updateModal.find('table'), adGroup.devices);

            updateModal.find('.submit-btn').on('click', function (e) {
                e.preventDefault();
                var table = $('#updateModal').find('table');
                var adGroup = getEntityFromNavigation('adgroup');
                var data = {};
                data.devices = getCheckboxesFromTable(table);
                putUpdateAdGroup(adGroup, data);
            });

            updateModal.modal('show');
        });

        entityField.find('.targeting-os .btn').on('click', function (e) {
            e.preventDefault();
            var updateModal = $('#updateModal');
            var adGroup = getEntityFromNavigation('adgroup');

            updateModal.html($("#targeting-os-template").html());
            fillTableWithCheckboxes(updateModal.find('table'), adGroup.operatingSystems);

            updateModal.find('.submit-btn').on('click', function (e) {
                e.preventDefault();
                var table = $('#updateModal').find('table');
                var adGroup = getEntityFromNavigation('adgroup');
                var data = {};
                data.operatingSystems = getCheckboxesFromTable(table);
                putUpdateAdGroup(adGroup, data);
            });

            updateModal.modal('show');
        });

        entityField.find('.targeting-schedule .btn').on('click', function (e) {
            e.preventDefault();
            var updateModal = $('#updateModal');
            var adGroup = getEntityFromNavigation('adgroup');

            updateModal.html($("#targeting-schedule-template").html());
            var table = updateModal.find('table');

            table.on('click', function () {
                var schedule = getScheduleFromTable($(this));
                updateModal.find('.schedule-list').html(scheduleListTemplate(schedule));
            });

            renderScheduleTable(table, adGroup.schedule);

            updateModal.find('.submit-btn').on('click', function (e) {
                e.preventDefault();
                var table = $('#updateModal').find('table');
                var adGroup = getEntityFromNavigation('adgroup');
                var data = {};
                data.schedule = getScheduleFromTable(table);
                putUpdateAdGroup(adGroup, data);
            });

            updateModal.modal('show');
        });

        function putUpdateAdGroup(adGroup, data) {
            $.ajax({
                method: "PUT",
                url: "v1/adgroups/" + adGroup.id,
                contentType: 'application/json',
                data: JSON.stringify(data)
            })
                .done(function(data) {
                    updateModal.modal('hide');
                    pushInNavigation('adgroup', data);
                    $(window).trigger('hashchange');
                })
                .fail(function (data) {
                    var alert = updateModal.find('.error-alert');
                    alert.html("Error: " + data.responseJSON.message);
                    alert.show();
                });
        }

        page.find('.table-title').text("Creatives");
        table.html(creativesTemplate(content));

        table.off('click').on('click', 'a', function (e) {
            e.preventDefault();
            var creativeId = $(e.target).closest('tr').data('index');
            var creative = getContentObjectById(creativeId);
            pushInNavigation('creative', creative);
            var advertiser = getEntityFromNavigation('advertiser');
            var campaign = getEntityFromNavigation('campaign');
            var adGroup = getEntityFromNavigation('adgroup');
            router.setRoute('/advertisers/' + advertiser.id + '/campaigns/' + campaign.id
                + '/adgroups/' + adGroup.id + '/creatives/' + creative.id);
        });

        page.find('.btn-create').on('click', function (e) {
            e.preventDefault();
            var createModal = $('#createModal');

            prepareModal(createModal, creativesEditTemplate(), function (e) {
                e.preventDefault();
                var adGroup = getEntityFromNavigation("adgroup");
                var form = createModal.find('form');
                if (validateForm(form)) {
                    var fileInput = $('#editCreativeContentFile');
                    var formData = getObjectFromForm(form);
                    formData.type = "IMAGE";

                    var fd = new FormData();
                    fd.append('form', JSON.stringify(formData));
                    fd.append('file', fileInput[0].files[0]);

                    $.ajax({
                        url: "v1/adgroups/" + adGroup.id + "/creatives",
                        data: fd,
                        processData: false,
                        contentType: false,
                        type: 'POST'
                    })
                        .done(function() {
                            createModal.modal('hide');
                            $(window).trigger('hashchange');
                        })
                        .fail(function (data) {
                            var alert = createModal.find('.error-alert');
                            alert.html("Error: " + data.responseJSON.message);
                            alert.show();
                        });
                }
            });

            createModal.modal('show');
        });

        table.on('click', '.btn-edit', function (e) {
            e.preventDefault();
            var updateModal = $('#updateModal');
            var id = $(this).closest('tr').data('index');
            var creative = getContentObjectById(id);

            prepareModal(updateModal, creativesEditTemplate(creative), function(e) {
                e.preventDefault();
                var form = updateModal.find('form');
                if (validateForm(form)) {
                    $.ajax({
                        method: "PUT",
                        url: "v1/creatives/" + id,
                        contentType: 'application/json',
                        data: JSON.stringify(getObjectFromForm(form))
                    })
                        .done(function() {
                            updateModal.modal('hide');
                            $(window).trigger('hashchange');
                        })
                        .fail(function (data) {
                            var alert = updateModal.find('.error-alert');
                            alert.html("Error: " + data.responseJSON.message);
                            alert.show();
                        });
                }
            });

            updateModal.modal('show');
        });

        table.on('click', '.btn-edit-content', function (e) {
            e.preventDefault();
            var updateModal = $('#updateModal');
            var id = $(this).closest('tr').data('index');
            var creative = getContentObjectById(id);

            prepareModal(updateModal, creativesEditContentTemplate(creative), function(e) {
                e.preventDefault();
                var form = updateModal.find('form');
                if (validateForm(form)) {
                    var fileInput = $('#updateCreativeContentFile');
                    var fd = new FormData();
                    fd.append('file', fileInput[0].files[0]);

                    $.ajax({
                        url: "v1/creatives/" + id + "/content",
                        data: fd,
                        processData: false,
                        contentType: false,
                        type: 'POST'
                    })
                        .done(function() {
                            createModal.modal('hide');
                            $(window).trigger('hashchange');
                        })
                        .fail(function (data) {
                            var alert = createModal.find('.error-alert');
                            alert.html("Error: " + data.responseJSON.message);
                            alert.show();
                        });
                }
            });

            updateModal.modal('show');
        });

        table.on('click', '.btn-delete', function (e) {
            e.preventDefault();
            var deleteModal = $('#deleteModal');
            var id = $(this).closest('tr').data('index');
            var creative = getContentObjectById(id);
            creative.type = "Creative";

            deleteModal.html(deleteModalTemplate(creative));

            deleteModal.find('#btn-delete-modal').on('click', function () {
                $.ajax({
                    method: "DELETE",
                    url: "v1/creatives/" + id
                })
                    .done(function() {
                        deleteModal.modal('hide');
                        $(window).trigger('hashchange');
                    })
                    .fail(function (data) {
                        var alert = deleteModal.find('.error-alert');
                        alert.html("Error: " + data.responseJSON.message);
                        alert.show();
                    });
            });

            deleteModal.modal('show');
        });

        swapPagesIfNeeded(page);
    }

    function renderSingleCreativePage(trackersData) {

        var createModal = $('#createModal');
        var updateModal = $('#updateModal');

        var creative = getEntityFromNavigation('creative');
        var data = {};
        data.creative = creative;
        data.trackers = trackersData;

        var page = $('.creative-page');

        page.html(singleCreativeTemplate(data));

        swapPagesIfNeeded(page);
    }

    function updatePaginationWithData(data) {
        if (data.hasOwnProperty("number")) {
            $.extend(true, pagination, data);
        } else {
            resetPagination();
        }
        updatePagination();
    }

    function updatePagination() {

        var pageParams = $('.page-params');
        var pager = pageParams.find('.pager');
        var previous = pager.find('.previous');
        var previousLink = previous.find('a');
        var next = pager.find('.next');
        var nextLink = next.find('a');

        var oldSize = pagination.size;
        var newSize = $('#page-size').find("option:selected").text();
        pagination.size = newSize;
        pagination.number = Math.floor(pagination.number * oldSize / newSize);

        var pagerLabel = pageParams.find('.pager-description');
        var start = pagination.number * pagination.size;
        pagerLabel.html("Showing ("
            + (start + 1) + "-" + (start + pagination.numberOfElements)
            + ") of " + pagination.totalElements);

        if (pagination.first) {
            previous.addClass('disabled');
            previousLink.attr('href', null);
            previousLink.off('click');
        } else {
            previous.removeClass('disabled');
            previousLink.attr('href', "#prev");
            previousLink.on('click', function (e) {
                e.preventDefault();
                pagination.number --;
                $(window).trigger('hashchange');
            })
        }

        if (pagination.last) {
            next.addClass('disabled');
            nextLink.attr('href', null);
            nextLink.off('click');
        } else {
            next.removeClass('disabled');
            nextLink.attr('href', "#next");
            nextLink.on('click', function (e) {
                e.preventDefault();
                pagination.number ++;
                $(window).trigger('hashchange');
            })
        }

    }

    function resetPagination() {
        pagination = {
            "first": true,
            "last": true,
            "number": 0,
            "numberOfElements": 0,
            "size": $('#page-size').find("option:selected").text(),
            "sort": {},
            "totalElements": 0,
            "totalPages": 0
        };
    }

	// Shows the error page.
	function renderErrorPage(){
		var page = $('.error-page');
        swapPagesIfNeeded(page);
	}

	function swapPagesIfNeeded(page) {
        if (!page.hasClass('visible')) {
            // Hide whatever page is currently shown.
            $('.main-content .page').removeClass('visible');
            page.addClass('visible');
        }
    }

    function prepareModal(targetModal, content, submitCallback) {
        targetModal.html(content);
        var form = targetModal.find('form');
        prepareNotNullFormFields(form);
        $('input[type=file]').bootstrapFileInput();
        $(form).on('change dp.change', function (e) {
            validateFormField($(e.target).closest('.form-group'));
        });
        $(form).on('submit', submitCallback);
        targetModal.find('.submit-btn').on('click', submitCallback);
    }

    function prepareNotNullFormFields(form) {
        var controls = form.find(".form-group.not-null");
        controls.each(function () {
            $(this).addClass('has-feedback');
            $(this).append($('#not-null-status-template').html());
            })
    }

    function validateForm(form) {
        var pass = true;
        var controls = form.find('.form-group');
        controls.each(function () {
            pass &= validateFormField($(this));
        });
        return pass;
    }

    function validateFormField(field) {
        var pass = true;

        if (field.hasClass('not-null')) {
            if (field.find('.form-control, input[type=file]').val() == '') {
                invalidField(field, "Field can not be empty");
                pass = false;
            }
        }

        var fileInput = field.find('input[type=file]');
        if (fileInput.length) {
            if (fileInput[0].files[0]) {
                if (fileInput[0].files[0].size > 1024 * 1024) {
                    invalidField(field, "File too large (maximum file size is 1Mb)");
                    pass = false;
                }
            }
        }

        if(pass) {
            validField(field);
        }

        return pass;

        function validField(field) {
            field.find('.help-block').text("");
            field.find('.form-control-feedback').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            field.removeClass('has-error');
            field.addClass('has-success');
        }

        function invalidField(field, text) {
            field.find('.help-block').text(text);
            field.find('.form-control-feedback').removeClass('glyphicon-ok').addClass('glyphicon-remove');
            field.removeClass('has-success');
            field.addClass('has-error');
        }
    }

    function getObjectFromForm(form) {
        var params = {};
        form.find('.form-control').each(function () {
            params[$(this).attr('name')] = $(this).val();
        });
        return params;
    }

    function getContentObjectById(id) {
        return content.filter(function (element) {
            return element.id == id;
        })[0];
    }

    function pushInNavigation(type, entity) {
        var flush = false;
        navigation.forEach(function (item) {
            if (flush) {
                item.entity = {};
            } else {
                if (item.type === type) {
                    if (item.entity.id != entity.id) {
                        flush = true;
                    }
                    item.entity = entity;
                }
            }
        });
    }

    function resetNavigation() {
        navigation.forEach(function (item) {
            item.entity = {};
            item.active = false;
        });
        updateNavigationBreadcrumbs();
    }

    function updateNavigation() {
        var args = arguments;
        var promises = [];
        navigation.forEach(function (item, i) {
            if (args[i]) {
                var path = 'v1/';
                item.active = true;
                if (item.entity.id == undefined || args[i] != item.entity.id) {
                    switch (item.type) {
                        case 'advertiser': {
                            path += ('advertisers/' + args[i]);
                            break;
                        }
                        case 'campaign': {
                            path += ('campaigns/' + args[i]);
                            break;
                        }
                        case 'adgroup': {
                            path += ('adgroups/' + args[i]);
                            break;
                        }
                        case 'creative': {
                            path += ('creatives/' + args[i]);
                            break;
                        }
                        default: break;
                    }

                    var promise = $.getJSON(path, function(data) {
                        item.entity = data;
                    });
                    promises.push(promise);
                }
            } else {
                item.active = false;
            }
        });
        $.when.apply($, promises).done(updateNavigationBreadcrumbs);
    }

    function updateNavigationBreadcrumbs() {
        var path = "#/advertisers";
        var bc = $('#navigation-breadcrumb');
        bc.html('');
        bc.append("<li><a href=" + path + ">Home</a></li>");
        for(var i = 0; i < navigation.length; i++) {
            var item = navigation[i];
            if (item.active) {
                var entityName = item.entity.name;
                path += ("/" + item.entity.id + item.subPath);
                bc.append("<li><a href=" + path + ">" + entityName + "</a></li>");
            }
        }
        var last = bc.find('li:last');

        last.html(last.find('a').html());
        last.addClass('active');
    }

    function getEntityFromNavigation(type) {
        return navigation.filter(function (element) {
            return element.type === type;
        })[0].entity;
    }

    function dateToTimestamp(date) {
        return moment(date, dateFormat).toISOString();
    }

    function formatCampaignDates(obj) {
        if (obj) {
            if ($.isArray(obj)) {
                obj.forEach(function (item) {
                    format(item)
                })
            } else {
                format(obj);
            }
        }

        function format(item) {
            item.startDate = moment(item.startDate).format(dateFormat);
            item.endDate = moment(item.endDate).format(dateFormat);
        }
    }

    function bindGlobalUIEvents() {

        $('#page-size').on('change', function (e) {
            e.preventDefault();
            updatePagination({});
            $(window).trigger('hashchange');
        });

        $('#search-input').on('change', function () {
            var url = window.location.hash;
            var index = window.location.hash.lastIndexOf('/search');
            if (index < 0) {
                index = url.length
            }
            router.setRoute(encodeURI(url.slice(1, index) + "/search?name=" + this.value));
        });

        $('.search-btn').on('click', function () {
            $('#search-input').trigger('change');
        });

        $('.clear-search-btn').on('click', function () {
            $('#search-input').val(null);
            var url = window.location.hash;
            var index = window.location.hash.lastIndexOf('/search');
            if (index < 0) {
                index = url.length
            }
            router.setRoute(url.slice(1, index));
        });

        $('#createModal, #updateModal').on('shown.bs.modal', function () {
            $(this).find('.form-control:first').focus()
        });

        $('#deleteModal').on('shown.bs.modal', function () {
            $(this).find('#btn-cancel-delete').focus()
        });

        var table = $('.main-page .table-responsive');

        table.on('show.bs.dropdown', function () {
            table.css( "overflow", "inherit" );
        });

        table.on('hide.bs.dropdown', function () {
            table.css( "overflow", "auto" );
        });
    }

    function updateStatusButtons(table) {
        var btn = table.find('.status-btn');
        btn.each(function () {
            var status = $(this).data('status');
            switch (status) {
                case 'DRAFT': {
                    $(this).addClass('btn-default');
                    break;
                }
                case 'ACTIVE': {
                    $(this).addClass('btn-success');
                    break;
                }
                case 'PENDING': {
                    $(this).addClass('btn-info');
                    break;
                }
                case 'PAUSED': {
                    $(this).addClass('btn-warning');
                    break;
                }
                case 'ENDED': {
                    $(this).addClass('btn-danger');
                    break;
                }
                default: break;
            }
        })
    }

    function fillTableWithCheckboxes(table, array) {
        var rows = table.find('tbody tr');
        array.forEach(function (item) {
            rows.each(function () {
                if ($(this).data('type') == item) {
                    $(this).find('input').attr('checked', true);
                }
            })
        })
    }

    function getCheckboxesFromTable(table) {
        var array = [];
        var rows = table.find('tbody tr');
        rows.each(function () {
            if ($(this).find('input').is(":checked")) {
                array.push($(this).data('type'))
            }
        });
        return array;
    }

    function renderScheduleTable(table, schedule) {
        var head = "<tr><th></th>";
        for(var i=0; i<25; i++) {
            if (i % 4 == 0) {
                head += "<th>" + i + "</th>"
            } else {
                head += "<th></th>"
            }
        }
        head += "<th></th></tr>";
        table.find('thead').html(head);

        var body = $('<tbody></tbody>');

        for(var i=0; i<7; i++) {
            var row = $('<tr></tr>');
            row.data('row', i);
            row.append($('<td class="day"></td>'));
            for(var j=0; j<24; j++) {
                var cell = $('<td></td>');
                cell.addClass('cell');
                cell.data('column', j);
                row.append(cell);
            }
            row.append($('<td class="toggle"><input type="checkbox"></td>'));
            body.append(row)
        }
        table.find('tbody').replaceWith(body);

        table.find('.cell').on('click', function () {
            $(this).toggleClass('enabled');
        });

        table.find('input').on('change', function () {
            var row = $(this).closest('tr');
            var cells = row.find('.cell');
            cells.toggleClass('enabled', $(this).is(':checked'));
            table.trigger('click');
        });

        fillScheduleTable(table, schedule);
    }

    function fillScheduleTable(table, schedule) {
        var rows = table.find('tbody tr');
        rows.each(function (i) {
            var label;
            var day;
            switch(i) {
                case 0: {
                    label = "M";
                    day = "MONDAY";
                    break;}
                case 1: {
                    label = "Tu";
                    day = "TUESDAY";
                    break;}
                case 2: {
                    label = "W";
                    day = "WEDNESDAY";
                    break;}
                case 3:{
                    label = "Th";
                    day = "THURSDAY";
                    break;}
                case 4: {
                    label = "F";
                    day = "FRIDAY";
                    break;}
                case 5: {
                    label = "Sa";
                    day = "SATURDAY";
                    break;}
                case 6: {
                    label = "Su";
                    day = "SUNDAY";
                    break;}
            }
            $(this).data('day', day);
            $(this).find('.day').html(label);
        });

        schedule.forEach(function (item) {
            rows.each(function () {
                if ($(this).data('day') == item.dayOfWeek) {
                    var cells = $(this).find('.cell');
                    item.hours.forEach(function (hours) {
                        for(var i = hours.startHour; i <= hours.endHour; i++) {
                            var cell = cells.get(i);
                            $(cell).addClass('enabled');
                        }
                    });
                    // check checkbox if all elements in row are enabled
                    $(this).find('input').attr('checked', $(this).find('.enabled').size() == 24);
                }
            });
        });

        // trigger rendering of schedule-list in modal
        table.trigger('click');
    }

    function getScheduleFromTable(table) {
        var schedule = [];
        var rows = table.find('tbody tr');

        rows.each(function () {
            var cells = $(this).find('.cell');

            var hours = [];
            var start = null;
            var end = null;
            for(var i = 0; i < 25; i++) {
                var cell = cells[i];
                if ($(cell).hasClass('enabled')) {
                    if (start == null) {
                        start = i;
                        end = i;
                    } else {
                        end = i;
                    }
                } else {
                    if (end != null) {
                        var interval = {};
                        interval.startHour = start;
                        interval.endHour = end;
                        hours.push(interval);
                        start = null;
                        end = null;
                    }
                }
            }

            if (hours.length != 0) {
                var day = {};
                day.dayOfWeek = $(this).data('day');
                day.hours = hours;
                schedule.push(day);
            }
        });

        return schedule;
    }
});